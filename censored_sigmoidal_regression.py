import numpy as np
from numba import jit, prange, int64
from copy import deepcopy
from sigmoid import SigmoidalRegression, sigFunction
from censored_gaussian import estimate
from scipy.stats import norm
from matplotlib import pyplot as plt
import time

## To get predicted onsets for censored subjects and plot
def impute_and_plot(S, onset, censored, correct_cov = True, dalpha = 0.5, onset_par = (90, 10), mode = 'average',
                    plot=True, figure = None):
    if S.Z is not None:
        Y = S.Y - np.dot(S.Z, S.param['v'])[:, None]
    else:
        Y = S.Y
    if correct_cov and S.Z is not None:
        Yplot = Y
    else:
        Yplot = S.Y

    if mode not in ('average', 'sample'):
        print('unrecognized mode: using "sample"')
        mode = 'sample'
    start = np.zeros(S.nsub, dtype=int)
    start[1:] = np.cumsum(S.nobs)[:-1]
    mu2 = (S.param['rho']/S.param['sigma'])**2
    onset_ = np.copy(onset)
    nobs = S.nobs
    age = S.t[:,0]
    param = S.getParam()
    min_prob_steps = 5
    col = np.ones(Y.shape[0])
    for k in range(S.nsub):
        if censored[k]:
            endk = start[k] + nobs[k]
            alpha = onset[k]
            gamma = []
            prob = []
            resmin = np.inf
            tau2 = 1 + nobs[k] * mu2
            col[start[k]:endk] = 2
            oldres = np.inf
            while True:
                gamma.append(alpha + dalpha / 2)
                res = Y[start[k]:endk, 0] - sigFunction(age[start[k]:endk] - gamma[-1], param['a'], param['b'],
                                                        param['c'], param['d'], param['tm'], param['delta'])
                res_b = res.sum() ** 2 / nobs[k]
                res_v = (res ** 2).sum() - res_b
                res = (res_v + res_b / tau2) / (2 * param['sigma'] ** 2)
                # res = ((res ** 2).sum() - mutau2 * (res.sum()) ** 2) / (2 * sigma ** 2)
                res += 0.5 * ((gamma[-1] - onset_par[0]) / onset_par[1]) ** 2
                prob.append(res)
                if len(prob) > min_prob_steps and res > oldres and res - resmin > 10:
                    break
                oldres = res
                if res < resmin:
                    resmin = res
                alpha += dalpha
            gamma = np.array(gamma)
            prob = dalpha * np.exp(-np.array(prob) + resmin)
            sumprob = prob.sum()
            prob /= sumprob
            lik = np.log(sumprob) -resmin
            if mode == 'average':
                onset_[k] = (gamma*prob).sum()
            elif mode == 'sample':
                j = np.random.choice(prob.shape[0], size=1, p=prob)[0]
                onset_[k] = gamma[j]
            # print(f'Creating {len(prob)} new observations with average onset {(gamma*prob).sum():.2f}')

    if plot:
        t = np.copy(S.t)
        for k in range(S.nsub):
            t[start[k]:start[k]+S.nobs[k]] -= onset_[k]
        if figure is None:
            figure = plt.figure('Predicted Onset')
        figure.clf()
        #ax = figure.add_subplot(111)
        kj = 0
        for k in range(S.nsub):
            tk = t[kj:kj+nobs[k],0]
            Yk = Yplot[kj:kj+nobs[k],0]
            if censored[k]:
                c = [0,0,1]
            else:
                c = [0.5, 0.5, 1]
            plt.plot(tk, Yk, marker='o', ms=1, c=c)
            kj += nobs[k]
        Y0 = S.predict(t, without_cov = correct_cov)
        plt.plot(t, Y0, '*', ms = 1, label='prediction', c='r')
        figure.legend()
        figure.canvas.draw_idle()
        figure.canvas.flush_events()
        time.sleep(.5)
    return onset_

def compute_likelihood(S, onset, censored, onset_par, dalpha = 0.5):
    if S.Z is not None:
        Y = S.Y - np.dot(S.Z, S.param['v'])[:, None]
    else:
        Y = S.Y
    start = np.zeros(S.nsub, dtype=int)
    start[1:] = np.cumsum(S.nobs)[:-1]
    mu2 = (S.param['rho']/S.param['sigma'])**2
    nobs = S.nobs
    N = Y.shape[0]
    age = S.t[:,0]
    param = S.getParam()
    min_prob_steps = 5
    col = np.ones(N)
    lik = N*np.log(param['sigma']) + 0.5 * np.log(1 + mu2*nobs).sum()
    for k in range(S.nsub):
        endk = start[k] + nobs[k]
        tau2 = 1 + nobs[k] * mu2
        if censored[k]:
            alpha = onset[k]
            gamma = []
            prob = []
            resmin = np.inf
            col[start[k]:endk] = 2
            oldres = np.inf
            while True:
                gamma.append(alpha + dalpha / 2)
                res = Y[start[k]:endk, 0] - sigFunction(age[start[k]:endk] - gamma[-1], param['a'], param['b'],
                                                        param['c'], param['d'], param['tm'], param['delta'])
                res_b = res.sum() ** 2 / nobs[k]
                res_v = (res ** 2).sum() - res_b
                res = (res_v + res_b / tau2) / (2 * param['sigma'] ** 2)
                # res = ((res ** 2).sum() - mutau2 * (res.sum()) ** 2) / (2 * sigma ** 2)
                res += 0.5 * ((gamma[-1] - onset_par[0]) / onset_par[1]) ** 2
                prob.append(res)
                if len(prob) > min_prob_steps and res > oldres and res - resmin > 10:
                    break
                oldres = res
                if res < resmin:
                    resmin = res
                alpha += dalpha
            prob = dalpha * np.exp(-np.array(prob) + resmin)
            sumprob = prob.sum()
            lik -= np.log(sumprob) -resmin - norm.logsf(onset[k], onset_par[0], onset_par[1])
        else:
            res = Y[start[k]:endk, 0] - sigFunction(age[start[k]:endk] - onset[k], param['a'], param['b'],
                                                        param['c'], param['d'], param['tm'], param['delta'])
            res_b = res.sum() ** 2 / nobs[k]
            res_v = (res ** 2).sum() - res_b
            lik += (res_v + res_b / tau2) / (2 * param['sigma'] ** 2)
            # res = ((res ** 2).sum() - mutau2 * (res.sum()) ** 2) / (2 * sigma ** 2)

    return -lik


def generateBootstrapSample(S, onset, censored, onset_par, replace = True, bootstrap_param = None, wild=True):
    #print('bootstrap', S.nsub, censored.shape)
    if bootstrap_param is None:
        bootstrap_param = S.param
    onset2 = impute_and_plot(S, onset, censored, onset_par=onset_par, plot=False, mode='sample')

    if S.Z is not None:
        Y = S.Y - np.dot(S.Z, S.param['v'])[:, None]
    else:
        Y = S.Y
    start = np.zeros(S.nsub, dtype=int)
    start[1:] = np.cumsum(S.nobs)[:-1]
    mu2 = (S.param['rho']/S.param['sigma'])**2
    nobs = S.nobs

    t = np.copy(S.t)
    for k in range(S.nsub):
        t[start[k]:start[k] + S.nobs[k]] -= onset2[k]
    Y0 = S.predict(t)

    R = Y - Y0
    residuals = np.zeros(Y.shape)
    for k in range(S.nsub):
        endk = start[k] + nobs[k]
        tau = 1/np.sqrt(1 + nobs[k] * mu2)
        meanR = np.mean(R[start[k]:endk])
        residuals[start[k]:endk] = R[start[k]:endk] + (tau-1)*meanR

    #print(residuals.shape)
    if wild:
        mr = np.mean(residuals)
        stdr = np.std(residuals)
        # print(mr, stdr)
        bootres = np.random.uniform(-np.sqrt(3), np.sqrt(3), size=Y.shape[0])
        bootres = mr + stdr * bootres[:, None]
        # print(np.mean(bootres), np.std(bootres))
    else:
        J = np.random.choice(Y.shape[0], Y.shape[0], replace = replace)
        bootres = residuals[J,:]


    Sboot = SigmoidalRegression()
    Sboot.setParam(bootstrap_param)
    mu2 = (bootstrap_param['rho']/bootstrap_param['sigma'])**2
    for k in range(S.nsub):
        endk = start[k] + nobs[k]
        tau = np.sqrt(1 + nobs[k] * mu2)
        meanR = np.mean(bootres[start[k]:endk])
        bootres[start[k]:endk] += (tau-1)*meanR
    bootY = Sboot.predict(t, S.Z) + bootres

    return bootY


@jit(nopython=True, parallel= False)
def get_EM_gamma(Y, age, onset, censored, Z, dalpha,
                 a, b, c, d, v, tm, delta, rho, sigma, mean_onset, sigma_onset):
    mu2 = (rho / sigma) ** 2
    if censored:
        nobs = Y.shape[0]
        alpha = onset
        gamma = []
        prob = []
        resmin = np.inf
        tau2 = 1 + nobs * mu2
        oldres = np.inf
        #sumprod = 0
        if Z is not None:
            Yres = Y[:, 0] - np.dot(Z, v)
        else:
            Yres = np.copy(Y[:,0])
        while True:
            gamma.append(alpha + dalpha / 2)
            res = Yres - sigFunction(age - gamma[-1], a, b, c, d, tm, delta)
            res_b = res.sum() ** 2 / nobs
            res_v = (res ** 2).sum() - res_b
            res = (res_v + res_b / tau2) / (2 * sigma ** 2)
            res += 0.5 * ((gamma[-1] - mean_onset) / sigma_onset) ** 2
            if res < resmin:
                resmin = res
            prob.append(res)
            prob_ = dalpha * np.exp(-np.array(prob) + resmin)
            sumprob = prob_.sum()
            if prob_[-1] < sumprob * 1e-8:
                #if len(prob) > min_prob_steps and res > oldres and res - resmin > 10:
                break
            alpha += dalpha
        gamma = np.array(gamma)
        prob = dalpha * np.exp(-np.array(prob) + resmin)
        sumprob = prob.sum()
        prob /= sumprob
        # lik += np.log(sumprob) - resmin - 0.5 * np.log(tau2)
        pmax = prob.max()
        J = np.nonzero(prob > 1e-6 * pmax)[0]
        gamma = gamma[J]
        prob = prob[J]
    else:
        gamma = np.array([onset])
        prob = np.array([1.])

    return gamma, prob


@jit(nopython=True, parallel= True)
def get_EM_gamma_loop(start, nobs, Y, age, onset, censored, Z, dalpha,
                 a, b, c, d, v, tm, delta, rho, sigma, mean_onset, sigma_onset):
    nsub = nobs.shape[0]
    all_gamma = [np.zeros(0)]*nsub
    all_prob = [np.zeros(0)]*nsub
    if Z is not None:
        for k in prange(nsub):
            endk = start[k] + nobs[k]
            gamma, prob = get_EM_gamma(Y[start[k]:endk], age[start[k]:endk], onset[k], censored[k],
                                       Z[start[k]:endk], dalpha,
                                       a, b, c, d, v, tm, delta,
                                       rho, sigma, mean_onset, sigma_onset)
            all_gamma[k] = gamma
            all_prob[k] = prob
    else:
        for k in prange(nsub):
            endk = start[k] + nobs[k]
            gamma, prob = get_EM_gamma(Y[start[k]:endk], age[start[k]:endk], onset[k], censored[k],
                                       None, dalpha,
                                       a, b, c, d, v, tm, delta,
                                       rho, sigma, mean_onset, sigma_onset)
            all_gamma[k] = gamma
            all_prob[k] = prob

    new_nsub = 0
    ntot = 0
    for k,g in enumerate(all_gamma):
        new_nsub += len(g)
        ntot += len(g)*nobs[k]

    ## M step
    ## extended dataset
    # new_nobs = np.zeros(new_nsub).astype(int)
    # kj = 0
    # ntot = 0
    # for k in range(nsub):
    #     for j in range(len(all_gamma[k])):
    #         ntot += nobs[k]
            # new_nobs[kj] = nobs[k]
            # kj += 1
    # ntot = new_nobs.sum()
    newY = np.zeros((ntot, 1))
    newAge = np.zeros(ntot)
    new_id = np.zeros(ntot, dtype=int64)
    weights = np.zeros(new_nsub)

    if Z is not None:
        newZ = np.zeros((ntot, Z.shape[1]))
    else:
        newZ = np.zeros((1,1))

    nk = 0
    kj = 0
    nkj = 0
    for k in range(nsub):
        for j in range(len(all_gamma[k])):
            for jj in range(nobs[k]):
                newY[nkj+jj, 0] = Y[kj+jj, 0]
                newAge[nkj+jj] = age[kj+jj] - all_gamma[k][j]
                new_id[nkj+jj] = 10000 * kj + nk + 1  # id[kj:kj+nobs[k]]
                if Z is not None:
                    newZ[nkj+jj, :] = Z[kj+jj, :]
            weights[nk] = all_prob[k][j]
            nkj += nobs[k]
            nk += 1
        kj += nobs[k]

    # if verb:
    #     print(f'Number of EM subjects: {nk} with {newY.shape} observations')

    return new_id, newY, newAge, newZ, weights


###
# Main function implementing the EM algorithm
###

def SR_EM_Default_options():
    options = {
        'decreasing': True,
        'leftFlat': False,
        'rightFlat': False,
        'random_effect': True,
        'dalpha':.5,
        'linear':False,
        'tolerance': 1e-3,
        'max_iter': 100,
        'max_iter_run': 100,
        'single_cp': False,
        'min_delta_time':1e-6,
        'optimize_quantile': False,
        'init_cp': None,
        'init_q': None,
        'timedisc': None,
        'verb': True
    }
    return options

def SR_EM_optimize(S0, onset, censored, onset_par = (90, 10), options = None, change_points = None):
    nsub = S0.nsub
    nobs = S0.nobs
    start = np.zeros(nsub, dtype=int)
    start[1:] = np.cumsum(nobs)[:-1]
    param = S0.getParam()
    oldParam = SigmoidalRegression.SigmoidParameters(param)
    if options['verb']:
        figure = plt.figure('Predicted Onset')
    else:
        figure = None
    for it in range(options['max_iter']):
        if options['verb']:
            print(f'EM Iteration {it+1}')
        ###
        # E step
        ###
        #print('iteration ', param)
        new_id, newY, newAge, newZ, weights = get_EM_gamma_loop(start, nobs, S0.Y, S0.t[:,0], onset, censored, S0.Z, options['dalpha'],
                                                                param['a'], param['b'], param['c'], param['d'], param['v'],
                                                                param['tm'], param['delta'], param['rho'], param['sigma'],
                                                                onset_par[0], onset_par[1])

        if S0.Z is None:
            newZ = None


        if options['verb']:
            print(f'Number of EM subjects: {weights.shape} with {newY.shape} observations')
        # Parameter update
        S = SigmoidalRegression(new_id, newY, newAge, Z=newZ, w=weights)
        S.setParam(param)
        if options['linear']:
            S.fit_linear(reff=options['random_effect'], firstRun=False, max_iter=options['max_iter_run'])
        else:
            S.fit(random_effect=options['random_effect'], normalize=False, Decreasing=options['decreasing'], leftFlat=options['leftFlat'],
                  rightFlat=options['rightFlat'], delta_lb=options['min_delta_time'], verb=False, single_cp=options['single_cp'],
                  time_lb=None, firstRun=False, max_iter=options['max_iter_run'], init_quantile=options['init_q'], change_points=change_points)

        #S.plot()
        param = S.getParam()
        S0.setParam(param)
        if options['verb']:
            true_lik = compute_likelihood(S0, onset, censored, onset_par)
            S0.printParam(message=f'parameters:  ')
            print(f'max slope: {np.abs(S0.slope_differential()):.4f}, {np.abs(S0.normalized_slope_differential()):.4f}')
            print(f'Likelihood:  {true_lik:.4f}')
        impute_and_plot(S0, onset, censored, dalpha=options['dalpha'], onset_par=onset_par, figure=figure,
                        mode='sample', plot = options['verb'])
        diff = oldParam.max_diff(param)
        if options['verb']:
            print(f'difference: {diff:.4f}')
        if diff < options['tolerance']:
            break
        oldParam = S.getParam()

    S0.likelihood = compute_likelihood(S0, onset, censored, onset_par)

    return S0

def SR_EM(id, Y, age, onset, censored, Z=None, onset_par = (90, 10), options = None, initParam = None):
    # uniqueId = np.unique(id)
    opt = SR_EM_Default_options()
    if options is not None:
        for k in options.keys():
            opt[k] = options[k]

    verb = opt['verb']
    best_q = 0.5
    if verb:
        print(f'EM algorithm. Total number of subjects: {onset.shape[0]}, Right-censored: {censored.sum()}')
    min_prob_steps = 50
    if verb:
        figure = plt.figure('Predicted Onset')
    else:
        figure = None

    ## Initialization
    S0 = SigmoidalRegression(id, Y, age, Z=Z)
    nsub = S0.nsub
    nobs = S0.nobs
    age_ = np.copy(age)
    start = np.zeros(nsub, dtype=int)
    start[1:] = np.cumsum(nobs)[:-1]
    # oldParam = SigmoidalRegression.SigmoidParameters(param)

    best_lik = -np.inf
    best_par = SigmoidalRegression.SigmoidParameters()
    if initParam is not None:
        S_ = SigmoidalRegression(id, Y, age_, Z=Z)
        S_.setParam(initParam)
        initLik = compute_likelihood(S_, onset, censored, onset_par)

    S_ = SigmoidalRegression(id, Y, age_, Z=Z)

    if verb:
        print(f'Initial likelihood: {best_lik:.4f}')
    #
    # else:
    #     tmin = np.zeros(nsub)
    #     tmax = np.zeros(nsub)
    #     kj = 0
    #     for k in range(nsub):
    #         tmin[k] = age[kj]
    #         tmax[k] = age[kj + nobs[k]-1]
    #         kj += nobs[k]
    #
    #     mm = onset_par[0]
    #     ss = onset_par[1]
    #     # mm, ss = estimate(onset, tmin, tmax, censored)
    #     kj = 0
    #     onset_guess = np.zeros(nsub)
    #     c = 0.5 * np.log(2*np.pi)
    #     for k in range(nsub):
    #         if censored[k]:
    #             min_val = (onset[k] - mm) /ss
    #             u = np.random.uniform(0,1)
    #             m = np.exp(-np.minimum(0.5 * min_val ** 2 + c + norm.logsf(min_val), 100)).sum()
    #             #m = norm.pdf(min_val) / (1-norm.cdf(min_val))
    #             #m = norm.ppf((1-u)*norm.cdf(min_val) + u)
    #
    #             # s2 = 1 - (m- min_val)*m
    #             # print(m, s2)
    #             onset_guess[k] = mm + ss * m
    #             # print(onset_guess - onset[k])
    #             #onset_guess[k] = (onset[k]+onset_guess)/2
    #             #onset_guess = onset[k]
    #             age_[kj:kj+nobs[k]] -= onset_guess[k]
    #         else:
    #             onset_guess[k] = onset[k]
    #             age_[kj:kj + nobs[k]] -= onset[k]
    #         kj += nobs[k]


    if not opt['linear']:
    #     S_.fit_linear(reff=opt['random_effect'])
    # #S.fit_linear(reff=random_effect)
    #     # param = S_.getParam()
    #     # if verb:
    #     #     S_.printParam(message='Initial parameters:  ')
    # else:
        param = S_.getParam()
        if opt['timedisc'] is None:
            timedisc = np.array([-10, 10])
            # best_q = 0.3
            # S_.fit(Decreasing=opt['decreasing'], normalize=False, leftFlat=opt['leftFlat'],
            #        random_effect=opt['random_effect'],
            #        single_cp=opt['single_cp'], time_lb=None, delta_lb=opt['min_delta_time'], init_quantile=best_q,
            #        init_cp=opt['init_cp'])
            # param = S_.getParam()
        else:
            timedisc = opt['timedisc']
        max_iter = opt['max_iter']
        verb = opt['verb']
        nt = len(timedisc)
        opt['max_iter'] = 5
        opt['verb'] = False
        # best_lik = -np.inf
        # best_par = SigmoidalRegression.SigmoidParameters()
        for k in range(nt):
            for l in range(k+1, nt):
                param.update_time_param(timedisc[k], timedisc[l])
                S0.setParam(param)
                S0 = SR_EM_optimize(S0, onset, censored, onset_par, options=opt, change_points=[param['t0'], param['t1']])
                if verb:
                    print(f"t0={param['t0']:.4f} t1={param['t1']:.4f} lik={S0.likelihood:.4f}")
                if S0.likelihood > best_lik:
                    best_lik = S0.likelihood
                    best_par.setParam(S0.getParam())
        opt['max_iter'] = max_iter
        opt['verb'] = verb
        S0.setParam(best_par)

    S0 = SR_EM_optimize(S0, onset, censored, onset_par, options=opt)
    if initParam is not None and S0.likelihood < initLik:
        S0.setParam(initParam)
        S0.likelihood = initLik

    return S0