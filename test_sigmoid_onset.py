import numpy as np
import pandas as pd
from numpy import dtype

from sigmoid import SigmoidalRegression, sigFunction
from censored_sigmoidal_regression import SR_EM
import matplotlib
matplotlib.use("QT5Agg")
import matplotlib.pyplot as plt
plt.ion()
generate_data = True
decreasing = True
mean_onset = 90
std_onset = 13
censored = True

if generate_data:
    nsub = 300
    nobs = 3 + np.random.choice(7, size=nsub)
    #nobs = 5 * np.ones(nsub, dtype=int)
    ncov = 1
    agemin = 50
    agemax = 80
    N = nobs.sum()
    param = {
        't0':-10,
        't1':5,
        'a':-200,
        'rho': 10,
        'sigma':1
    }
    if decreasing:
        param['c'] = 0.5
        param['d'] = 0.5
        param['b'] = -0.6
    else:
        param['c'] = -0.025
        param['d'] = -0.025
        # c=0
        # d=0
        param['b'] = 0.03
    param['v'] = np.array([2.5])
    param['tm'] = (param['t0']+param['t1'])/2
    param['delta'] = param['t1'] - param['t0']
    true_param = SigmoidalRegression.SigmoidParameters()
    true_param.setParam(param)
    print('True parameters: ', true_param)
    # res = f'True param: a={a:.4f} b={b:.4f} c={c:.4f} d={d:.4f} t0={t0:.4f} t1={t1:.4f} '
    # res += f'rho={rho:.4f} sigma={sigma:.4f}'
    # res += f' v={v[0]:.4f} '
    # print(res)

    age = np.zeros(N)
    t = np.zeros(N)
    Z = np.zeros((N,ncov))
    kj_ = 0
    noise = param['sigma'] * np.random.normal(0, 1, (N,1))
    onset_true = np.random.normal(mean_onset, std_onset, (nsub, 1))
    onset = np.copy(onset_true)
    id = np.zeros(N, dtype=int)
    rc = np.zeros(nsub, dtype=int)
    rc_ = np.zeros(N)
    onset_ = np.zeros(N)
    onset_true_ = np.zeros(N)
    for k in range(nsub):
        kj = kj_
        reff = param['rho']*np.random.normal(0,1,1)
        age[kj] = agemin + np.random.rand(1)[0] * (agemax-agemin)
        t[kj] = age[kj] - onset_true[k, 0]
        noise[kj] += reff
        id[kj] = k
        kj += 1
        npersub = nobs[k]
        for j in range(1, npersub):
            age[kj] = age[kj-1] + 2
            t[kj] = age[kj] - onset_true[k, 0]
            noise[kj] += reff
            id[kj] = k
            if k < nsub/2:
                Z[kj] = 1
            kj += 1
        if age[kj-1] < onset_true[k]:
            onset[k] = age[kj-1]
            rc[k] = True
        kj = kj_
        for j in range(npersub):
            onset_true_[kj] = onset_true[k, 0]
            onset_[kj] = onset[k, 0]
            rc_[kj] = rc[k]
            kj += 1
        kj_ = kj


    Y = sigFunction(t, param['a'], param['b'], param['c'], param['d'], param['tm'], param['delta'])[:, None]  \
        + np.dot(Z,param['v'])[:, None] + noise
    data = dict()
    data['age'] = age
    data['onset'] = onset_
    data['censored'] = rc_
    data['onset_true'] = onset_true_
    data['id'] = id
    data['Y'] = Y[:,0]
    for j in range(ncov):
        data[f'Z{j+1}'] = Z[:,j]
    df = pd.DataFrame(data)
    df.to_csv('data_onset.csv')
else:
    df = pd.read_csv('data_onset.csv')
    id = df['id'].to_numpy(dtype=int)
    age = df['age'].to_numpy()
    rc_ = df['censored'].to_numpy()
    onset_ = df['onset'].to_numpy()
    onset_true_ = df['onset_true'].to_numpy()
    t = age - onset_true_
    Y = df['Y'].to_numpy()[:, None]
    ncov = len(df.keys()) - 6
    N = Y.shape[0]
    Z = np.zeros((N,ncov))
    for i in range(ncov):
        Z[:,i] = df[f'Z{i+1}'].to_numpy()
    true_param = None




if censored:
    SR_EM_OPTIONS = {
        'decreasing': True,
        'leftFlat': False,
        'rightFlat': False,
        'random_effect': True,
        'dalpha': .25,
        'linear': False,
        'tolerance': 1e-3,
        'max_iter': 100,
        'max_iter_run': 100,
        'single_cp': False,
        'min_delta_time': .5,
        'verb': True
    }

    foo, J = np.unique(id, return_index=True)
    onset = onset_[J]
    rc = rc_[J]
    # S = SR_EM(id, Y, age, onset, rc, Z=Z, onset_par=(mean_onset, std_onset), options=SR_EM_OPTIONS)
    # S.printParam(message='Linear model')
    S = SR_EM(id, Y, age, onset, rc, Z=Z, onset_par=(mean_onset, std_onset), options=SR_EM_OPTIONS)
    S.printParam(message='General model')
else:
    S = SigmoidalRegression(id, Y, t, Z=Z, true_param=true_param)
    S.setParam(true_param)
    print(f'Likelihood for true param: {S.objective()}')
    S.fit(random_effect=True, normalize=False, Decreasing=decreasing, leftFlat=False, verb=True, firstRun=True,
          single_cp=False, time_lb=None, init_quantile=0.25)
    S.printParam()
    S.plot(figure = 'Final Plot')
    plt.ioff()
    plt.show()

