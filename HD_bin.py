#import predict_pca
import sys
import numpy as np
import pandas as pd
import pickle
from matplotlib import pyplot as plt
from datetime import datetime, timedelta
#import seaborn as sns
from sklearn.linear_model import LinearRegression
from pylab import *
from scipy.optimize import curve_fit
from scipy.optimize import minimize
import random
import csv

from scipy.optimize import LinearConstraint, Bounds, NonlinearConstraint, BFGS

SMALL_SIZE = 26
MEDIUM_SIZE = 30
BIGGER_SIZE = 36

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=16)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

def remove_Subj_less3(df, id_key):
    SubjIDList = list(df[id_key])
    for subj_id in SubjIDList:
        if SubjIDList.count(subj_id)<3:
            df = df[df[id_key]!=subj_id]
    return df

def get_Track_vol_features(df_merged_multscan, features, feature_full, opt='cap', WB_Norm=True, WB_Norm_features=None, IC_features=None):
    #drop controls
    df_merged_multscan = df_merged_multscan[df_merged_multscan['incl02'] != 0]
    df_merged_multscan = df_merged_multscan.dropna(subset = ['incl02'])
    
    # recalculate the cap score to match with Predict project
    df_merged_multscan['cap'] = df_merged_multscan['Age at Scan'] * (df_merged_multscan['CagLarge'] - 33.66)
    X = []
    y = []
    gp = []
    wb = []
    ic = []
    for subj in df_merged_multscan['Subject ID'].unique():
#         print(subj)
        df_patient = df_merged_multscan.loc[df_merged_multscan['Subject ID'] == subj]
        if opt is 'cap':
            caps = df_patient['cap']
        elif opt is 'age':
            caps = df_patient['Age at Scan']
        
        vols = np.zeros(size(df_patient[[features[0] + '_L', features[0]+ '_R']].mean(axis = 1)))
        for feature in features:
            vols += df_patient[[feature + '_L', feature + '_R']].mean(axis = 1)
#         print(len(vols))

        if WB_Norm_features is not None:
            vols_WB = np.array(df_patient[list(set(WB_Norm_features) & set(df_patient.columns))]).sum(axis = 1)
        else :
            vols_WB = np.ones_like(vols)
            
        if IC_features is not None:
            vols_IC = np.array(df_patient[list(set(IC_features) & set(df_patient.columns))]).sum(axis = 1)
        else :
            vols_IC = np.ones_like(vols)
            
        if WB_Norm:
            vols = vols/vols_WB
        
        if vols.size > 1:
            dvol_max = np.max(np.abs(np.gradient(vols)))
        else:
            dvol_max = 0

        if np.isnan(caps).any(): # excluded nan cap subject
            print('exclude subject ID ' + str(subj) + " due to nan CAP scores")
        elif (np.array(caps)>800).any(): # excluded nan cap subject
            print('exclude subject ID ' + str(subj) + " due to CAP scores > 800")
#         elif (np.array(caps)<40).any(): # excluded nan cap subject
#             print('exclude subject ID ' + str(subj) + " due to CAP scores < 40")          
        elif np.isnan(vols).any(): # excluded nan vols subject    
            print('exclude subject ID ' + str(subj) + " due to nan volume")
        else:
            vols = vols.tolist()
            caps = caps.tolist() 
            vols_WB = vols_WB.tolist()
            group = df_patient['incl02'].iloc[0].tolist()
            if len(caps) > 0:
                caps, vols, vols_WB, vols_IC = (list(t) for t in zip(*sorted(zip(caps, vols, vols_WB, vols_IC))))
                gp.append(group)
                X.append(caps)
                y.append(vols)
                wb.append(vols_WB)
                ic.append(vols_IC)
    return X, y, gp, wb, ic

def get_Predict_vol_features(df_all, features, feature_full, opt='cap', WB_Norm=True, WB_Norm_features=None, IC_features=None):
    
    ## exclude control subjects
    df_all = df_all[df_all['cap_grp'] != 'cont']
    
    X = []
    y = []
    gp = []
    wb = []
    ic = []
    for subj in df_all.subjid.unique():
        df_patient = df_all.loc[df_all['subjid'] == subj]
        if opt is 'cap':
            caps = df_patient['cap'] 
        elif opt is 'age':
            caps = df_patient['age']
            
        vols = np.zeros(size(df_patient[[features[0] + '_L', features[0]+ '_R']].mean(axis = 1)))
        
        for feature in features:
            vols += df_patient[[feature + '_L', feature + '_R']].mean(axis = 1)
            
        if WB_Norm_features is not None:
            vols_WB = np.array(df_patient[list(set(WB_Norm_features) & set(df_patient.columns))]).sum(axis = 1)
        else :
            vols_WB = np.ones_like(vols)

        if IC_features is not None:
            vols_IC = np.array(df_patient[list(set(IC_features) & set(df_patient.columns))]).sum(axis = 1)
        else :
            vols_IC = np.ones_like(vols)
            
        if WB_Norm:
            vols = vols/vols_WB
            
            
        if vols.size > 1:
            dvol_max = np.max(np.abs(np.gradient(vols)))
        else:
            dvol_max = 0

        if np.isnan(caps).any(): # excluded nan cap subject
            print('exclude subject ID ' + str(subj) + " due to nan CAP scores")
        elif (np.array(caps)>800).any(): # excluded nan cap subject
            print('exclude subject ID ' + str(subj) + " due to CAP scores > 800")
#         elif (np.array(caps)<40).any(): # excluded nan cap subject
#             print('exclude subject ID ' + str(subj) + " due to CAP scores < 40")           
        elif np.isnan(vols).any(): # excluded nan vols subject    
            print('exclude subject ID ' + str(subj) + " due to nan volume")
        else:
            vols = vols.tolist()
            caps = caps.tolist() 
            group = df_patient['cap_grp'].iloc[0]
            vols_WB = vols_WB.tolist()
            
            if len(caps) > 0:
                caps, vols, vols_WB, vols_IC = (list(t) for t in zip(*sorted(zip(caps, vols, vols_WB, vols_IC))))
                gp.append(group)
                X.append(caps)
                y.append(vols)
                wb.append(vols_WB)
                ic.append(vols_IC)
    return X, y, gp, wb, ic

def get_ImageHD_vol_features(df_all, features, feature_full, opt='cap', WB_Norm=True, WB_Norm_features=None, IC_features=None):
    
    ## exclude control subjects
    df_all = df_all[df_all['Group'] != '1']
    df_all = df_all.dropna(subset=['CAG repeats'])
    X = []
    y = []
    gp = []
    wb = []
    ic = []
    for subj in df_all.ID.unique():
        df_patient = df_all.loc[df_all['ID'] == subj]
        if opt is 'cap':
            caps = (df_patient['CAG repeats'] - 33.66) * df_patient['Age']
        elif opt is 'age':
            caps = df_patient['Age']
            
        vols = np.zeros(size(df_patient[[features[0] + '_L', features[0]+ '_R']].mean(axis = 1)))
        
        for feature in features:
            vols += df_patient[[feature + '_L', feature + '_R']].mean(axis = 1)
            
        if WB_Norm_features is not None:
            vols_WB = np.array(df_patient[list(set(WB_Norm_features) & set(df_patient.columns))]).sum(axis = 1)
        else :
            vols_WB = np.ones_like(vols)

        if IC_features is not None:
            vols_IC = np.array(df_patient[list(set(IC_features) & set(df_patient.columns))]).sum(axis = 1)
        else :
            vols_IC = np.ones_like(vols)
            
        if WB_Norm:
            vols = vols/vols_WB
            
            
        if vols.size > 1:
            dvol_max = np.max(np.abs(np.gradient(vols)))
        else:
            dvol_max = 0

        if np.isnan(caps).any(): # excluded nan cap subject
            print('exclude subject ID ' + str(subj) + " due to nan CAP scores")
        elif (np.array(caps)>800).any(): # excluded nan cap subject
            print('exclude subject ID ' + str(subj) + " due to CAP scores > 800")
#         elif (np.array(caps)<40).any(): # excluded nan cap subject
#             print('exclude subject ID ' + str(subj) + " due to CAP scores < 40")           
        elif np.isnan(vols).any(): # excluded nan vols subject    
            print('exclude subject ID ' + str(subj) + " due to nan volumne")
        else:
            vols = vols.tolist()
            caps = caps.tolist() 
            group = df_patient['Group'].iloc[0]
            vols_WB = vols_WB.tolist()
            
            if len(caps) > 0:
                caps, vols, vols_WB, vols_IC = (list(t) for t in zip(*sorted(zip(caps, vols, vols_WB, vols_IC))))
                gp.append(group)
                X.append(caps)
                y.append(vols)
                wb.append(vols_WB)
                ic.append(vols_IC)
    return X, y, gp, wb, ic

def get_Track_control_vol_features(df_merged_multscan, features, feature_full, opt='age', age_margin=0, WB_Norm=True, WB_Norm_features=None, IC_features=None):
    df_merged_multscan = df_merged_multscan[df_merged_multscan['incl02'] == 0]
    df_merged_multscan = df_merged_multscan.dropna(subset = ['incl02'])
    
    # recalculate the cap score to match with Predict project
    df_merged_multscan['cap'] = df_merged_multscan['Age at Scan'] * (df_merged_multscan['CagLarge'] - 33.66)

    X = []
    y = []
    gp = []
    wb = []
    ic = []
    for subj in df_merged_multscan['Subject ID'].unique():
#         print(subj)
        df_patient = df_merged_multscan.loc[df_merged_multscan['Subject ID'] == subj]
        if opt is 'cap':
            caps = df_patient['cap']
        elif opt is 'age':
            caps = df_patient['Age at Scan']
            
        vols = np.zeros(size(df_patient[[features[0] + '_L', features[0]+ '_R']].mean(axis = 1)))
        for feature in features:
            vols += df_patient[[feature + '_L', feature + '_R']].mean(axis = 1)
        
        if WB_Norm_features is not None:
            vols_WB = np.array(df_patient[list(set(WB_Norm_features) & set(df_patient.columns))]).sum(axis = 1)
        else :
            vols_WB = np.ones_like(vols)

        if IC_features is not None:
            vols_IC = np.array(df_patient[list(set(IC_features) & set(df_patient.columns))]).sum(axis = 1)
        else :
            vols_IC = np.ones_like(vols)
            
        if WB_Norm:
#             print(vols)
#             print(vols_WB)
            vols = vols/vols_WB 
        
        vols = vols.tolist()
        caps = caps.tolist() 
        vols_WB = vols_WB.tolist()
        
        if age_margin !=0:
            vols = [vols[_] for _ in range(len(caps)) if caps[_] <= age_margin]
            if WB_Norm_features is not None:
                vols_WB = [vols_WB[_] for _ in range(len(caps)) if caps[_] <= age_margin]
            vols_IC = [vols_IC[_] for _ in range(len(caps)) if caps[_] <= age_margin]
            caps = [age for age in caps if age <= age_margin]
            
        if len(vols)  > 1:
            dvol_max = np.max(np.abs(np.gradient(vols)))
        else:
            dvol_max = 0

        if np.isnan(caps).any(): # excluded nan cap subject
            print('exclude subject ID ' + str(subj) + " due to nan CAP scores")
        elif (np.array(caps)>800).any(): # excluded nan cap subject
            print('exclude subject ID ' + str(subj) + " due to CAP scores > 800")
#         elif (np.array(caps)<40).any(): # excluded nan cap subject
#             print('exclude subject ID ' + str(subj) + " due to CAP scores < 40")         
        elif np.isnan(vols).any(): # excluded nan vols subject    
            print('exclude subject ID ' + str(subj) + " due to nan volume")
        else:

            group = df_patient['incl02'].iloc[0].tolist()
            if len(caps) > 0:
                caps, vols, vols_WB, vols_IC = (list(t) for t in zip(*sorted(zip(caps, vols, vols_WB, vols_IC))))
                gp.append(group)
                X.append(caps)
                y.append(vols)
                wb.append(vols_WB)
                ic.append(vols_IC)
    return X, y, gp, wb, ic

def get_Predict_control_vol_features(df_all, features, feature_full, opt='age', age_margin=0, WB_Norm=True, WB_Norm_features=None, IC_features=None):
 ## exclude control subjects
    df_all = df_all[df_all['cap_grp'] == 'cont']
    
    X = []
    y = []
    gp = []
    wb = []
    ic = []
    for subj in df_all.subjid.unique():
        df_patient = df_all.loc[df_all['subjid'] == subj]
        if opt is 'cap':
            caps = df_patient['cap'] 
        elif opt is 'age':
            caps = df_patient['age']
            
        vols = np.zeros(size(df_patient[[features[0] + '_L', features[0]+ '_R']].mean(axis = 1)))
        for feature in features:
            vols += df_patient[[feature + '_L', feature + '_R']].mean(axis = 1)
        
        if WB_Norm_features is not None:
            vols_WB = np.array(df_patient[list(set(WB_Norm_features) & set(df_patient.columns))]).sum(axis = 1)
        else :
            vols_WB = np.ones_like(vols)

        if IC_features is not None:
            vols_IC = np.array(df_patient[list(set(IC_features) & set(df_patient.columns))]).sum(axis = 1)
        else :
            vols_IC = np.ones_like(vols)
        
        if WB_Norm:
            vols = vols/vols_WB
            
        vols = vols.tolist()
        caps = caps.tolist() 
        vols_WB = vols_WB.tolist()
        
        if age_margin !=0:
            vols = [vols[_] for _ in range(len(caps)) if caps[_] <= age_margin]
            if WB_Norm_features is not None:
                vols_WB = [vols_WB[_] for _ in range(len(caps)) if caps[_] <= age_margin]
            vols_IC = [vols_IC[_] for _ in range(len(caps)) if caps[_] <= age_margin]
            caps = [age for age in caps if age <= age_margin]
            
            
        if len(vols) > 1:
            dvol_max = np.max(np.abs(np.gradient(vols)))
        else:
            dvol_max = 0
            
        if np.isnan(caps).any(): # excluded nan cap subject
            print('exclude subject ID ' + str(subj) + " due to nan CAP scores")
        elif (np.array(caps)>800).any(): # excluded nan cap subject
            print('exclude subject ID ' + str(subj) + " due to CAP scores > 800")
#         elif (np.array(caps)<40).any(): # excluded nan cap subject
#             print('exclude subject ID ' + str(subj) + " due to CAP scores < 40")          
        elif np.isnan(vols).any(): # excluded nan vols subject    
            print('exclude subject ID ' + str(subj) + " due to nan volume")
        else:
            group = df_patient['cap_grp'].iloc[0]
            if len(caps) > 0:
                caps, vols, vols_WB, vols_IC = (list(t) for t in zip(*sorted(zip(caps, vols, vols_WB, vols_IC))))
                gp.append(group)
                X.append(caps)
                y.append(vols)
                wb.append(vols_WB)
                ic.append(vols_IC)
    return X, y, gp, wb, ic

def get_ImageHD_control_vol_features(df_all, features, feature_full, opt='age', age_margin=0, WB_Norm=True, WB_Norm_features=None, IC_features=None):
 ## exclude control subjects
    df_all = df_all[df_all['Group'] == '1']
    
    X = []
    y = []
    gp = []
    wb = []
    ic = []
    for subj in df_all.ID.unique():
        df_patient = df_all.loc[df_all['ID'] == subj]
        if opt is 'cap':
            caps = df_patient['CAG repeats']
        elif opt is 'age':
            caps = df_patient['Age']
            
        vols = np.zeros(size(df_patient[[features[0] + '_L', features[0]+ '_R']].mean(axis = 1)))
        for feature in features:
            vols += df_patient[[feature + '_L', feature + '_R']].mean(axis = 1)
        
        if WB_Norm_features is not None:
            vols_WB = np.array(df_patient[list(set(WB_Norm_features) & set(df_patient.columns))]).sum(axis = 1)
        else :
            vols_WB = np.ones_like(vols)

        if IC_features is not None:
            vols_IC = np.array(df_patient[list(set(IC_features) & set(df_patient.columns))]).sum(axis = 1)
        else :
            vols_IC = np.ones_like(vols)
        
        if WB_Norm:
            vols = vols/vols_WB
            
        vols = vols.tolist()
        caps = caps.tolist() 
        vols_WB = vols_WB.tolist()
        
        if age_margin !=0:
            vols = [vols[_] for _ in range(len(caps)) if caps[_] <= age_margin]
            if WB_Norm_features is not None:
                vols_WB = [vols_WB[_] for _ in range(len(caps)) if caps[_] <= age_margin]
            vols_IC = [vols_IC[_] for _ in range(len(caps)) if caps[_] <= age_margin]
            caps = [age for age in caps if age <= age_margin]
            
            
        if len(vols) > 1:
            dvol_max = np.max(np.abs(np.gradient(vols)))
        else:
            dvol_max = 0
            
        if np.isnan(caps).any(): # excluded nan cap subject
            print('exclude subject ID ' + str(subj) + " due to nan CAP scores")
        elif (np.array(caps)>800).any(): # excluded nan cap subject
            print('exclude subject ID ' + str(subj) + " due to CAP scores > 800")
#         elif (np.array(caps)<40).any(): # excluded nan cap subject
#             print('exclude subject ID ' + str(subj) + " due to CAP scores < 40")       
        elif np.isnan(vols).any(): # excluded nan vols subject    
            print('exclude subject ID ' + str(subj) + " due to nan volume")
        else:
            group = df_patient['Group'].iloc[0]
            if len(caps) > 0:
                caps, vols, vols_WB, vols_IC = (list(t) for t in zip(*sorted(zip(caps, vols, vols_WB, vols_IC))))
                gp.append(group)
                X.append(caps)
                y.append(vols)
                wb.append(vols_WB)
                ic.append(vols_IC)
    return X, y, gp, wb, ic


def si_f2(t, t0=200, t1=600):
    t_bar = (t0 + t1)/2
    return t-t_bar

def si_f3(t, t0=200, t1=600):
    delta = t1 - t0
    condlist = [t < t0, (t >= t0) & (t < t1), t >= t1]
    funclist = [lambda t: -delta*(t-t0), lambda t: (t-t0)*(t-t1), lambda t: delta*(t-t1)]
    return np.piecewise(t, condlist, funclist)

def si_f4(t, t0=200, t1=600):
    delta = t1 - t0
    t_bar = (t0 + t1)/2
    condlist = [t < t0, (t >= t0) & (t < t1), t >= t1]
    funclist = [lambda t: delta**2*(t-t0)/2, lambda t: (t-t0)*(t-t1)*(t-t_bar), lambda t: delta**2*(t-t1)/2]
    return np.piecewise(t, condlist, funclist)

def f_new(t, a, b, c, d, t0=200, t1=600):
    t_bar = (t0 + t1)/2
    return a + b*si_f2(t, t0=t0, t1=t1) + c*si_f3(t,t0=t0, t1=t1) + d*si_f4(t,t0=t0, t1=t1)

def f_linear(t, a, b, t0=200, t1=600):
    return a + b*si_f2(t, t0=t0, t1=t1)

def f_flatsig(t, a, b, t0=200, t1=600):
    delta = t1-t0
    d = -b*2/(delta**2)
#     print('flagsig')
#     print(d)
    return a + b*si_f2(t, t0=t0, t1=t1) + d*si_f4(t,t0=t0, t1=t1)

def f_flatsig_leftonly(t, a, b, c, t0=200, t1=600):
    delta = t1-t0
    d = -b*2/(delta**2)+c*2/delta
#     print('flagsig_leftonly check non positive')
#     print(d+2*b/delta**2+2*c/delta)
    
#     print('flagsig_leftonly')
#     print(d)
    return a + b*si_f2(t, t0=t0, t1=t1) + c*si_f3(t,t0=t0, t1=t1) + d*si_f4(t,t0=t0, t1=t1)

def fit_fun(t, x, fit_model='cubic_sig'):
    
    if fit_model is 'cubic_sig':
        if len(x) < 6:
            print('For cubic_sig model, x should contain at least 6 parameters for a, b, c, d, t0, t1.')
        else:
            return f_new(t, x[0], x[1], x[2], x[3], t0=x[4], t1=x[5])
        
    elif fit_model is 'linear':
        if len(x) < 2:
            print('For linear model, x should contain at least two parameters for a, b.')
        else:
            return f_linear(t, x[0], x[1])
            
    elif fit_model is 'flat_sig':
        if len(x) < 4:
            print('For flat_sig model, x should contain at least 4 parameters for a, b, t0, t1.')
        else:
            return f_flatsig(t, x[0], x[1], t0=x[2], t1=x[3])
        
    elif fit_model is 'left_flat_sig':
        if len(x) < 5:
            print('For left_flat_sig model, x should contain at least 5 parameters for a, b, c, t0, t1.')
        else:
            return f_flatsig_leftonly(t, x[0], x[1], x[2], t0=x[3], t1=x[4])
        
    else:
        print('Wrong fit_model name, it should be cubic_sig, linear, flat_sig, left_flat_sig.')
        
# def r_kj(y_kj, t_kj, a, b, c, d, t0=200, t1=600, v=None, z_k=None, fit_fun=f_new):
#     if v is None or z_k is None:
#         return y_kj-fit_fun(t_kj, a, b, c, d, t0=t0, t1=t1)
#     else:
#         return y_kj-fit_fun(t_kj, a, b, c, d, t0=t0, t1=t1) - np.sum(v*z_k)

# def r_kj_sq(y_kj, t_kj, a, b, c, d, t0=200, t1=600, v=None, z_k=None):
#     return r_kj(y_kj, t_kj, a, b, c, d, t0=t0, t1=t1, v=v, z_k=z_k)**2


def r_kj(y_kj, t_kj, x, fit_model='cubic_sig', v=None, z_k=None):
    if v is None or z_k is None:
        return y_kj-fit_fun(t_kj, x, fit_model=fit_model)
    else:
#         print(v*z_k)
        return y_kj-fit_fun(t_kj, x, fit_model=fit_model) - np.sum(v*z_k)

def tou_mk_sq(rho, sigma, mk):
    return rho**2/(sigma**2+mk*rho**2)
# def tou_mk(rho, sigma, mk):
#     return np.sqrt(rho**2/(sigma**2+mk*rho**2))

def split_x(x, fit_model='cubic_sig'):
    if fit_model is 'cubic_sig':
        idx = 6
    elif fit_model is 'linear':
        idx = 2
    elif fit_model is 'flat_sig':
        idx = 4
    elif fit_model is 'left_flat_sig':
        idx = 5
    else: 
        print('input dimension errors!')
        
    x_par = x[0:idx]
    rho = x[idx]
    sigma = x[idx+1]
    if len(x) > idx+2:
        v = x[idx+2:]
    else :
        v = None
    return x_par, rho, sigma, v


def plot_Track_vols(X_t, y_t, gp_t):
    p1 = None
    p2 = None
    for _ in range(len(X_t)):
        caps = X_t[_]
        vols = y_t[_]
        if gp_t[_] == 1:
            p1, = plt.plot(X_t[_], y_t[_], marker = 'o', color = 'steelblue', label = 'Track-HD: Premanifest', alpha = 0.5, markersize = 5)
        else:
            p2, = plt.plot(X_t[_], y_t[_], marker = 'o', color = 'darkblue', label = 'Track-HD: Early', alpha = 0.5, markersize = 5)

    return p1, p2

def plot_Track_control_vols(X_t, y_t):
    p1 = None
    for _ in range(len(X_t)):
        caps = X_t[_]
        vols = y_t[_]
        p1, = plt.plot(X_t[_], y_t[_], marker = 'o', color = 'cyan', label = 'Track-HD: control', alpha = 0.5, markersize = 5)
    return p1

    
def plot_Predict_vols(X_p, y_p, gp_p):
    p3 = None
    p4 = None
    for _ in range(len(X_p)):
        caps = X_p[_]
        vols = y_p[_]
        if gp_p[_] == 'low' or gp_p[_] == 'med':
            p3, = plt.plot(caps, vols, marker = 'o', color = 'firebrick', label = 'Predict-HD: Low/Medium', alpha = 0.5)
        else:
            p4, = plt.plot(caps, vols, marker = 'o', color = 'darkred', label = 'Predict-HD: High', alpha = 0.7)
    return p3, p4
    
def plot_Predict_control_vols(X_p, y_p):
    p1 = None
    for _ in range(len(X_p)):
        caps = X_p[_]
        vols = y_p[_]
        p1, = plt.plot(X_p[_], y_p[_], marker = 'o', color = 'lightsalmon', label = 'Predict-HD: control', alpha = 0.5, markersize = 5)
    return p1


def plot_ImageHD_vols(X_i, y_i, gp_i):
    p1 = None
    p2 = None
    for _ in range(len(X_i)):
        caps = X_i[_]
        vols = X_i[_]
        if gp_i[_] == '2':
            p1, = plt.plot(X_i[_], y_i[_], marker = 'o', color = 'limegreen', label = 'Image-HD: Pre-HD', alpha = 0.5, markersize = 5)
        else:
            p2, = plt.plot(X_i[_], y_i[_], marker = 'o', color = 'darkgreen', label = 'Image-HD: Symp-HD', alpha = 0.5, markersize = 5)

    return p1, p2

def plot_ImageHD_control_vols(X_i, y_i):
    p1 = None
    for _ in range(len(X_i)):
        caps = X_i[_]
        vols = y_i[_]
        p1, = plt.plot(X_i[_], y_i[_], marker = 'o', color = 'chartreuse', label = 'Image-HD: control', alpha = 0.5, markersize = 5)
    return p1

def aligned_controls_cap(X):
    return [[x*5.6 for x in y] for y in X]

def f_obj(x, y, t, fit_model='cubic_sig', z=None):
    n_y = len(y) 
    N_y = sum( [ len(y_k) for y_k in y])
    n_t = len(t) 
    N_t = sum( [ len(t_k) for t_k in t])
    if n_y == n_t and N_y == N_t:
        N = N_y
        n = n_y
    else:
        print('observation and time points t dont match')
    
    if fit_model is 'cubic_sig':
        if len(x) < 8:
            print('For cubic_sig model, x should contain at least six parameters for a, b, c, d, t0, t1, rho and sigma.')
        else:
            x_par, rho, sigma, v = split_x(x, fit_model=fit_model)
        
    elif fit_model is 'linear':
        if len(x) < 4:
            print('For linear model, x should contain at least two parameters for a, b, rho and sigma.')
        else:
            x_par, rho, sigma, v = split_x(x, fit_model=fit_model)
            
    elif fit_model is 'flat_sig':
        if len(x) < 6:
            print('For flat_sig model, x should contain at least two parameters for a, b, t0, t1, rho and sigma.')
        else:
            x_par, rho, sigma, v = split_x(x, fit_model=fit_model)
        
    elif fit_model is 'left_flat_sig':
        if len(x) < 7:
            print('For left_flat_sig model, x should contain at least three parameters for a, b, c, t0, t1, rho and sigma.')
        else:
            x_par, rho, sigma, v = split_x(x, fit_model=fit_model)
    else :
        print('input dimension errors!')
        
    if z is not None:
        if all([len(_)==len(z[0]) for _ in z]) is False:
            print('z input dimension errors!')
            print(all([len(_)==len(z[0]) for _ in z]))
            print([len(_) for _ in z])
            
    if z is not None:
        if all([len(_)==len(v) for _ in z]) is False:
            print('v input dimension errors!')
            print(len(v))
            
    K_constant = n/2*np.log10(rho**2) + (N-n)/2*np.log10(sigma**2)
    
#     print(y)
#     print(t)
    
#     print(x)
#     print(x_par)
#     print(rho)
#     print(sigma)
#     print(v)
    
    for k in range(n):
        y_k = np.array(y[k])
        t_k = np.array(t[k])
        if z is not None:
            z_k = np.array(z[k])
        else:
            z_k = None
        mk = len(y_k)
        
        tou_mk_sq_v = tou_mk_sq(rho, sigma, mk)
        
#         print(v)
#         print(z_k)
        rkj = r_kj(y_k, t_k, x_par, fit_model=fit_model, v=v, z_k=z_k)
        K_constant = K_constant - np.log10(tou_mk_sq_v)/2 + np.sum(rkj**2)/2/(sigma**2) - (np.sum(rkj)**2)*tou_mk_sq_v/2/(sigma**2)
    return K_constant
    
