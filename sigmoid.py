import sys
import numpy as np
try:
    import cupy as cp
except:
    pass
import scipy.linalg as LA
from scipy.optimize import minimize, Bounds, NonlinearConstraint, LinearConstraint, minimize_scalar
import copy
from numba import jit, prange
import matplotlib.pyplot as plt
import time
import warnings
warnings.filterwarnings("ignore", message="delta_grad == 0.0. Check if the approximated function is linear.")


def fit_two_steps(y):
    errmin = np.inf
    kmin = 0
    for k in range(1,y.shape[0]-1):
        y1 = y[:k, :].mean(axis=0)
        y2 = y[k:, :].mean(axis=0)
        err = ((y[:k, :] - y1[:, None])**2).sum() + ((y[k:, :] - y2[:, None])**2).sum()
        if err < errmin:
            errmin =err
            kmin = k
    return kmin
@jit(nopython=True, parallel= False)
def sigFunction(t, a, b, c, d, tm, delta):
    t0 = tm-delta/2
    t1 = tm+delta/2
    Jbefore = np.nonzero(t<t0)[0]
    Jafter = np.nonzero(t>t1)[0]
    Jduring = np.nonzero(np.logical_and(t>=t0, t<=t1))[0]
    tduring = t[Jduring]
    phi1 = np.ones(t.shape)
    phi2 = t - tm
    phi3 = np.zeros(t.shape)
    phi3[Jbefore] = 1
    phi3[Jduring] = (tduring-t1)**2/delta**2
    phi3 *= t-t0
    phi4 = np.zeros(t.shape)
    phi4[Jduring] = (tduring-t0)**2/delta**2
    phi4[Jafter] = 1
    phi4 *= t-t1
    f = a*phi1 + b*phi2 + c*phi3 + d*phi4

    return f

def _objective(nobs, Y, t, Z, a, b, c, d, tm, delta, rho2, sigma2, v=None, reff=True):
    t0 = tm-delta/2
    t1 = tm+delta/2
    before = t<t0
    after = t>t1
    during = np.logical_and(t>=t0, t<=t1)
    tduring = t[during]
    phi1 = np.ones(t.shape)
    phi2 = t - tm
    phi3 = np.zeros(t.shape)
    phi3[before] = 1
    phi3[during] = (tduring-t1)**2/delta**2
    phi3 *= t-t0
    phi4 = np.zeros(t.shape)
    phi4[during] = (tduring-t0)**2/delta**2
    phi4[after] = 1
    phi4 *= t-t1
    f = a*phi1 + b*phi2 + c*phi3 + d*phi4
    r = Y - f
    if Z is not None:
        r -= np.dot(Z, v)[:, None]
    nsub = nobs.shape[0]
    ntot = Y.shape[0]
    rpart = (r**2).sum()
    obj = (ntot * np.log(sigma2) + rpart/sigma2)/2
    if reff:
        tau2 = rho2 /(sigma2+nobs*rho2)
        rm = np.zeros(nsub)
        kj = 0
        for k in range(nsub):
            for j in range(nobs[k]):
                rm[k] += r[kj]
                kj += 1
        rcorr = (tau2*(rm**2)).sum()
        obj += nsub*(np.log(rho2) - np.log(sigma2))/2 - rcorr/(2*sigma2) - np.log(tau2).sum()/2

    return obj

@jit(nopython=True, parallel=True)
def _objective_and_gradient_linear(nobs, Y, t, Z, a, b, rho2, sigma2, v=None, tm=None, delta=None):
    N = t.shape[0]
    if tm is None or delta is None:
        tm = (np.quantile(t, 0.25) + np.quantile(t, 0.75))/2
        delta = (np.quantile(t, 0.25) - np.quantile(t, 0.75))

    phi1 = np.ones(t.shape)
    phi2 = t - tm
    f = a*phi1 + b*phi2
    r = Y - f
    if Z is not None:
        r -= np.dot(Z, v)
    nsub = nobs.shape[0]
    ntot = Y.shape[0]
    tau2 = rho2 /(sigma2+nobs*rho2)
    rm = np.zeros(nsub)
    kj = 0
    for k in range(nsub):
        for j in range(nobs[k]):
            rm[k] += r[kj]
            kj += 1
    rpart = (r**2).sum() - (tau2*(rm**2)).sum()
    obj = (-np.log(tau2).sum() + nsub*np.log(rho2) + (ntot-nsub) * np.log(sigma2) + rpart/sigma2)/2
    gradr = r
    kj=0
    for k in range(nsub):
        for j in range(nobs[k]):
            gradr[kj] -= rm[k] * tau2[k]
            kj += 1
    grada = -gradr.sum()/sigma2
    gradb = -(gradr*phi2).sum()/sigma2
    gradrho2 = 0.5 * (-(1 - nobs*tau2).sum()/rho2 + nsub / rho2 - ((tau2*rm)**2).sum()/rho2**2)
    gradsigma2 = 0.5 * (- tau2.sum()/rho2 + (N-nsub)/sigma2 - rpart /sigma2**2 + ((tau2*rm)**2).sum()/(sigma2*rho2))
    if Z is not None:
        gradv = - np.dot(gradr, Z)/sigma2
    else:
        gradv = None

    grad = np.zeros(4)
    grad[0] = grada
    grad[1] = gradb
    grad[2] = gradrho2
    grad[3] = gradsigma2
    if Z is not None:
        grad = np.concatenate((grad, gradv), axis = 0)

    # print(f'obj = {obj}')
    #
    # eps = 1e-6
    # var = eps*np.random.normal(0, 1, size=1)[0]
    # obj2 = _objective(nobs, Y, t, Z, a, b, c, d, t0, t1+var, rho2, sigma2, v)
    # print(f'{(obj2 - obj)/eps}, {gradt1*var/eps}')

    return obj, grad


@jit(nopython=True, parallel=True)
def _objective_and_gradient(nobs, Y, t, Z, x, w, variances=None, change_points = None,
                            single_cp = False, reff=True, with_gradient=True):
    a = x[0]
    b = x[1]
    c = x[2]
    if single_cp:
        d = 0
        n0 = 3
    else:
        d = x[3]
        n0 = 4

    if change_points is None:
        fixed_cp = False
        tm = x[n0]
        delta = x[n0 + 1]
        t0 = tm - delta / 2
        t1 = tm + delta / 2
        n1 = n0 + 2
    else:
        fixed_cp = True
        t0 = change_points[0]
        t1 = change_points[1]
        tm = (t0+t1)/2
        delta = t1 - t0
        n1 = n0


    if variances is None:
        fixVariances = False
        if reff:
            rho2 = x[n1]
            sigma2 = x[n1+1]
            nv = n1 +2
        else:
            rho2 = 0
            sigma2 = x[n1]
            nv = n1 + 1
    else:
        fixVariances = True
        if reff:
            rho2 = variances[0]**2
            sigma2 = variances[1]**2
        else:
            rho2 = 0
            sigma2 = variances[1]**2
        nv = n1

    if Z is not None:
        v = x[nv:]
    else:
        v=np.empty(0)

    before = t<t0
    after = t>t1
    during = np.logical_and(t>=t0, t<=t1)
    Jbefore = np.nonzero(before)[0]
    Jafter = np.nonzero(after)[0]
    Jduring = np.nonzero(during)[0]
    tduring = t[Jduring]
    phi1 = np.ones(t.shape)
    phi2 = t - tm
    phi3 = np.zeros(t.shape)
    phi3[Jbefore] = 1
    phi3[Jduring] = (tduring-t1)**2/delta**2
    phi3 *= t-t0
    phi4 = np.zeros(t.shape)
    phi4[Jduring] = (tduring-t0)**2/delta**2
    phi4[Jafter] = 1
    phi4 *= t-t1
    f = a*phi1 + b*phi2 + c*phi3 + d*phi4
    r = Y - f
    if Z is not None:
        U = Z@v
        for j in range(r.shape[0]):
            r[j,0] -= U[j]
    nsub = nobs.shape[0]
    ntot = Y.shape[0]
    N = (nobs*w).sum()
    mu2 = rho2/sigma2
    wgradr = np.zeros((ntot,1))
    rm = None
    tau2 = np.zeros(nsub)
    if reff:
        tau2 = 1+nobs*mu2
        mutau2 = mu2/tau2
        rm = np.zeros(nsub)
        r2m = np.zeros(nsub)
        kj = 0
        for k in range(nsub):
            nk = nobs[k]
            for j in range(nk):
                r2m[k] += r[kj,0]**2
                rm[k] += r[kj,0]
                kj += 1
        rpart = (w*r2m).sum()
        rcorr = (w*mutau2*(rm**2)).sum()
        rpart -= rcorr
        obj = (N * np.log(sigma2) + rpart / sigma2) / 2 + (w*np.log(tau2)).sum()/2
        if with_gradient:
            wgradr = np.zeros((ntot, 1))
            kj=0
            for k in range(nsub):
                for j in range(nobs[k]):
                    wgradr[kj, 0] = w[k] * (r[kj,0] - rm[k] * mutau2[k])
                    kj += 1
    else:
        r2m = np.zeros(nsub)
        kj = 0
        for k in range(nsub):
            nk = nobs[k]
            for j in range(nk):
                r2m[k] += r[kj,0]**2
                kj += 1
        rpart = (w*r2m).sum()
        obj = (ntot * np.log(sigma2) + rpart / sigma2) / 2
        if with_gradient:
            wgradr = np.zeros((ntot, 1))
            kj=0
            for k in range(nsub):
                for j in range(nobs[k]):
                    wgradr[kj, 0] = w[k] * r[kj,0]
                    kj += 1

    if with_gradient:
        grada = -(wgradr).sum()/sigma2
        gradb = -(wgradr*phi2).sum()/sigma2
        gradc = -(wgradr*phi3).sum()/sigma2
        if single_cp:
            gradd = 0
        else:
            gradd = -(wgradr*phi4).sum()/sigma2
        gradrho2 = 0
        gradsigma2= 0
        if not fixVariances:
            if reff:
                ntau2 = 0
                taurm = 0
                for ii in prange(nsub):
                    ntau2 += w[ii]*nobs[ii]/tau2[ii]
                    taurm += w[ii]*(rm[ii]/tau2[ii])**2
                gradmu2 = 0.5 * (ntau2 - taurm/sigma2)
                gradrho2 = gradmu2/sigma2
                gradsigma2 = 0.5*(N - rpart /sigma2 - gradmu2/sigma2)/sigma2
            else:
                gradsigma2 = 0.5 * (N - rpart / sigma2) / sigma2

        if Z is not None:
            gradv = - np.dot(Z.T, wgradr)[:,0]/sigma2
        else:
            gradv = np.empty(0)

        graddelta = 0
        gradtm = 0
        if not fixed_cp:
            dphi2delta = 0
            dphi2tm = -1
            dphi3delta = 0.5 * before - (during * ((t - t1) / delta**2)) * ((t-t0) + 2*(t-t0)*(t-t1)/delta - (t-t1)/2)
            dphi3tm= (-1)*before - during * ((t - t1)/delta**2)*(2*(t-t0) + (t-t1))
            dphi4delta = (-0.5) * after - during * ((t - t0)/delta**2) *(-(t-t1) + 2*(t-t0)*(t-t1)/delta + (t-t0)/2)
            dphi4tm= (-1)*after - during * ((t - t0)/delta**2)*(2*(t-t1) + (t-t0))


            graddelta = - (wgradr*(dphi2delta*b + dphi3delta*c + dphi4delta*d)).sum()/sigma2
            gradtm = - (wgradr*(dphi2tm*b + dphi3tm*c + dphi4tm*d)).sum()/sigma2

        grad = np.zeros(nv)
        grad[0] = grada
        grad[1] = gradb
        grad[2] = gradc
        if not single_cp:
            grad[3] = gradd
        if not fixed_cp:
            grad[n0] = gradtm
            grad[n0+1] = graddelta
        if not fixVariances:
            if reff:
                grad[n1] = gradrho2
                grad[n1+1] = gradsigma2
            else:
                grad[n1] = gradsigma2
        if Z is not None:
            grad = np.concatenate((grad, gradv), axis = 0)
        return obj, grad
    else:
        return obj, np.zeros(nv)

#@jit(nopython=True, parallel=True)
def _objective_and_gradient_variance(nobs, resid, x, reff=True):
    if reff:
        rho2 = x[0]
        sigma2 = x[1]
    else:
        rho2 = 0
        sigma2 = x[0]

    nsub = nobs.shape[0]
    ntot = resid.shape[0]
    rpart = (resid**2).sum()
    gradr = np.copy(resid)
    obj = (ntot * np.log(sigma2) + rpart/sigma2)/2
    rm = None
    if reff:
        tau2 = rho2 /(sigma2+nobs*rho2)
        rm = np.zeros(nsub)
        kj = 0
        for k in range(nsub):
            for j in range(nobs[k]):
                rm[k] += resid[kj]
                kj += 1
        rcorr = (tau2*(rm**2)).sum()
        rpart -= rcorr
        obj += nsub*(np.log(rho2) - np.log(sigma2))/2 -  rcorr/(2*sigma2)
        kj=0
        for k in range(nsub):
            for j in range(nobs[k]):
                gradr[kj] -= rm[k] * tau2[k]
                kj += 1
        obj -= np.log(tau2).sum()/2
    else:
        tau2 = 0
    gradrho2 = 0
    if reff:
        gradrho2 = 0.5 * (-(1 - nobs*tau2).sum()/rho2 + nsub / rho2 - ((tau2*rm)**2).sum()/rho2**2)
        gradsigma2 = 0.5 * (- tau2.sum()/rho2 + (ntot-nsub)/sigma2 - rpart /sigma2**2 + ((tau2*rm)**2).sum()/(sigma2*rho2))
    else:
        gradsigma2 = 0.5 * (nobs / sigma2 - rpart / sigma2 ** 2)

    if reff:
        grad = np.zeros(2)
        grad[0] = gradrho2
        grad[1] = gradsigma2
    else:
        grad = np.zeros(1)
        grad[0] = gradsigma2

    #print(f'obj = {obj}, rho = {np.sqrt(rho2):.4f}, sigma = {np.sqrt(sigma2):.4f}')
    return obj, grad

@jit(nopython=True, parallel=True)
def precomp_variances(nobs, Y, t, Z, w, a, b, c, d, tm, delta, v):
    residuals = Y - sigFunction(t, a, b, c, d, tm, delta)
    nsub = nobs.shape[0]
    if Z is not None:
         residuals -= (Z @ v)[:, None]
    r2m = np.zeros(nsub)
    N = (w * nobs).sum()
    rm = np.zeros(nsub)
    kj = 0
    for k in range(nsub):
        for j in range(nobs[k]):
            r2m[k] += residuals[kj,0] ** 2
            rm[k] += residuals[kj,0]
            kj += 1

    rm2 = w * rm ** 2 / nobs
    r2 = (w * r2m).sum() - rm2.sum()
    #print(w, nobs, r2, rm2)

    return N, rm2, r2


@jit(nopython=True, parallel=True)
def fungrad_variances(x, nobs, w, N, rm2, r2):
    tau = 1 + x[0] * nobs
    s2 = r2 + (rm2 / tau).sum()
    u = N * np.log(r2 + (rm2 / tau).sum()) + (w * np.log(tau)).sum()
    g = (N / s2) * (rm2 * nobs / tau ** 2).sum() + (w * nobs / tau).sum()
    return u, g


def default_param():
    res = {
        'a': 0,
        'b': 0,
        'c': 0,
        'd': 0,
        't0': 0,
        't1': 1,
        'tm': 0.5,
        'delta': 1,
        'rho': 1,
        'sigma': 1,
        'v': None
        }
    return res

class SigmoidalRegression:
    class SigmoidParameters(dict):
        def __init__(self, param=None):
            super().__init__()
            param0 = default_param()
            for k in param0.keys():
                self[k] = param0[k]
            if param is not None:
                for k in param.keys():
                    self[k] = param[k]

        def setParam(self, param):
            for k in param.keys():
                self[k] = param[k]

        def max_diff(self, param):
            res = - np.inf
            for k in self.keys():
                if np.isscalar(self[k]):
                    res = max(np.fabs(self[k] - param[k]), res)
                else:
                    res = max(np.fabs(self[k] - param[k]).max(), res)
            return res

        def update_time_param(self, t0, t1):
            tm = self['tm']
            self['t0'] = t0
            self['t1'] = t1
            self['tm'] = (t0+t1)/2
            self['delta'] = t1 - t0
            self['a'] += self['b'] * (self['tm'] - tm)
        def __repr__(self):
            res = ''
            for k in ['a', 'b', 'c', 'd', 't0', 't1', 'rho', 'sigma']:
                res += k + f'={self[k]:.4f} '

            if self['v'] is not None:
                for i in range(self['v'].size):
                    res += f" v[{i}]: {self['v'][i]:.4f}"
            return res

    def __init__(self, id= None, Y=None, t = None, Z = None, w=None, true_param = None):
        self.param = self.SigmoidParameters()
        if Z is None:
            self.param['v'] = None
        else:
            self.param['v'] = np.zeros(Z.shape[1])
        self.maxiter = 100000
        #self.method = 'COBYQA'
        self.withJac = True
        self.method = 'trust-constr'
        if id is not None:
            self.initFit(id, Y, t, Z, w)
        self.likelihood = 0
        self.linear_likelihood = 0
        self.true_param = copy.deepcopy(true_param)


    def getParam(self):
        return self.param

    def setParam(self, param):
        self.param = copy.deepcopy(param)

    def setParam_(self, x, withVariances=True, reff=True, single_cp = False, with_cp = True):
        if single_cp:
            for i, k in enumerate(['a', 'b', 'c']):
                self.param[k] = x[i]
            n0 = 3
        else:
            for i,k in enumerate(['a', 'b', 'c', 'd']):
                self.param[k] = x[i]
            n0 = 4

        if with_cp:
            self.param['tm'] = x[n0]
            self.param['delta'] = x[n0+1]
            n0 += 2
            self.param['t0'] = self.param['tm'] - self.param['delta']/2
            self.param['t1'] = self.param['tm'] + self.param['delta']/2

        if withVariances:
            if reff:
                self.param['rho'] = np.sqrt(x[n0])
                self.param['sigma'] = np.sqrt(x[n0+1])
                nv = n0+2
            else:
                self.param['rho'] = 0
                self.param['sigma'] = np.sqrt(x[n0])
                nv = n0+1
        else:
            nv = n0
        if self.Z is not None:
            self.param['v'] = x[nv:]
        else:
            self.param['v'] = None


    def slope_differential(self):
        u = self.param['c']  + 2*self.param['d']
        v = self.param['d'] + 2*self.param['c']
        if np.abs(u+v) < 1e-8:
            return 0
        else:
            s = (u*(2*v-u)**2 + v*(2*u-v)**2)/(3*(u+v)**2)
            return s

    def normalized_slope_differential(self):
        return np.abs(self.slope_differential()) * (self.param['t1'] - self.param['t0']) \
            / np.sqrt(self.param['rho'] ** 2 + self.param['sigma'] ** 2)
    def setParamFromLinear_(self, x):
        self.param['a'] = x[0]
        self.param['b'] = x[1]
        self.param['rho'] = np.sqrt(x[2])
        self.param['sigma'] = np.sqrt(x[3])
        if x.size>4:
            self.param['v'] = x[4:]
        else:
            self.param['v'] = None

    def getVariable(self, withVariances=True, reff = True, single_cp = False, with_cp = True):
        if single_cp:
            d = 0
            n0 = 3
        else:
            n0 = 4

        if with_cp:
            n1 = n0 + 2
        else:
            n1 = n0

        if withVariances:
            if reff:
                nv = n1 + 2
            else:
                nv = n1 + 1
        else:
            nv = n1


        if self.param['v'] is None:
            x = np.zeros(nv)
        else:
            x = np.zeros(nv + self.param['v'].size)

        if single_cp:
            for i, k in enumerate(['a', 'b', 'c']):
                x[i] = self.param[k]
        else:
            for i,k in enumerate(['a', 'b', 'c', 'd']):
                x[i] = self.param[k]

        if with_cp:
            x[n0] = self.param['tm']
            x[n0+1] = self.param['delta']

        if withVariances:
            if reff:
                x[n1] = self.param['rho']**2
                x[n1+1] = self.param['sigma']**2
            else:
                x[n1] = self.param['sigma'] ** 2
        if self.param['v'] is not None:
            x[nv:] = self.param['v']
        return x

    def getLinearVariable(self):
        if self.param['v'] is None:
            x = np.zeros(4)
        else:
            x = np.zeros(4 + self.param['v'].size)

        x[0] = self.param['a']
        x[1] = self.param['b']
        x[2] = self.param['rho']**2
        x[3] = self.param['sigma']**2
        if self.param['v'] is not None:
            x[8:] = self.param['v']
        return x

    def objective(self, single_cp = False):
        x = self.getVariable(single_cp=single_cp)
        obj = _objective_and_gradient(self.nobs, self.Y, self.t, self.Z, x, self.w, single_cp=single_cp, with_gradient=False)
        return obj[0]

    def printParam(self, message=''):
        res = message + self.param.__repr__()
        print(res)

    def predict(self, t=None, Z = None, without_cov = False):
        if t is None:
            t = self.t
        if Z is None:
            Z = self.Z
        pred = sigFunction(t, self.param['a'], self.param['b'], self.param['c'], self.param['d'],
                           self.param['tm'], self.param['delta'])
        if not without_cov and Z is not None:
            pred += np.dot(Z, self.param['v'])[:, None]
        return pred

    def generate_bootstrap(self):
        J = np.random.choice(self.nsub, self.nsub, replace=True)
        start = np.zeros(self.nsub, dtype=int)
        start[1:] = np.cumsum(self.nobs)[:-1]
        end = start + self.nobs
        nobs = self.nobs[J]
        N = nobs.sum()
        tb = np.zeros((N,1))
        idb = np.zeros(N, dtype=int)
        if self.Z is None:
            Zb = None
        else:
            Zb = np.zeros((N, self.Z.shape[1]))
        kj = 0
        for j in range(self.nsub):
            k = J[j]
            tb[kj:kj+nobs[j],0] = self.t[start[k]:end[k],0]
            if self.Z is not None:
                Zb[kj:kj+nobs[j],:] = self.Z[start[k]:end[k],:]
            idb[kj:kj+nobs[j]] = j
            kj += nobs[j]

        noise = self.param['sigma'] * np.random.normal(0, 1, (N, 1))
        Yb = self.predict(tb, Z=Zb)
        kj=0
        for k in range(self.nsub):
            reff = self.param['rho'] * np.random.normal(0, 1, 1)
            noise[kj] += reff
            kj += 1
            for j in range(1, nobs[k]):
                noise[kj] += reff
                kj += 1
        Yb += noise
        return idb, tb, Yb, Zb

    def plot(self, correct_cov = True, figure = 'sigmoid'):
        if correct_cov and self.Z is not None:
            Y = self.Y[:,0] - np.dot(self.Z, self.param['v'])
        else:
            Y = self.Y
        fig = plt.figure(figure)
        plt.clf()
        plt.scatter(self.t, Y, c=self.expand_array(self.w), label='original data' )
        Y = self.predict(self.t, without_cov=correct_cov)
        plt.plot(self.t, Y, '.', label='prediction')
        plt.legend()
        fig.canvas.draw_idle()
        fig.canvas.flush_events()
        time.sleep(.5)

    def initFit(self, id, Y, t, Z=None, w=None):
        self.id = id
        if Y.ndim == 1:
            self.Y = np.copy(Y)[:, None]
        else:
            self.Y = np.copy(Y)
        if t.ndim==1:
            self.t = np.copy(t)[:, None]
        else:
            self.t = np.copy(t)
        if Z is None:
            self.Z = None
        elif Z.ndim ==1:
            self.Z = np.copy(Z)[:, None]
        else:
            self.Z = np.copy(Z)
        uniqueId = np.unique(id)
        self.nsub = uniqueId.shape[0]
        self.nobs = np.zeros(self.nsub, dtype=int)
        for k in range(self.nsub):
            self.nobs[k] = np.array(id == uniqueId[k]).sum()
        if w is not None:
            self.w = np.copy(w) #* self.Y.shape[0]/(w*self.nobs).sum()
        else:
            self.w = np.ones(self.nsub)


    def expand_array(self, w):
        nk = 0
        N = self.t.shape[0]
        w_star = np.zeros((N, 1))
        for k in range(self.nsub):
            w_star[nk:nk+self.nobs[k]] = w[k]
            nk += self.nobs[k]
        return w_star



    def fit_variances(self, reff= True, verb = False, initvar = None):
        if not reff:
            residuals = self.Y - self.predict(self.t)
            # if self.Z is not None:
            #     residuals -= (self.Z @ self.param.v)[:, None]
            r2m = np.zeros(self.nsub)
            N = (self.w * self.nobs).sum()
            kj = 0
            for k in range(self.nsub):
                for j in range(self.nobs[k]):
                    r2m[k] += residuals[kj]**2
                    kj += 1
            return (0, (self.w*r2m).sum()/N)
        else:
            N, rm2, r2 = precomp_variances(self.nobs, self.Y, self.t, self.Z, self.w,
                                           self.param['a'], self.param['b'], self.param['c'], self.param['d'],
                                           self.param['tm'], self.param['delta'], self.param['v'])

        # rm = np.zeros(self.nsub)
        # kj = 0
        # for k in range(self.nsub):
        #     for j in range(self.nobs[k]):
        #         r2m[k] += residuals[kj]**2
        #         rm[k] += residuals[kj]
        #         kj += 1
        #
        # rm2 = self.w*rm**2/self.nobs
        # r2 = (self.w*r2m).sum() - rm2.sum()
        def fun(x):
            tau = 1+x*self.nobs
            u = N * np.log(r2 + (rm2/tau).sum()) + (self.w*np.log(tau)).sum()
            return u

        def fungrad(x):
            return fungrad_variances(x, self.nobs, self.w, N, rm2, r2)
            # tau = 1+x[0]*self.nobs
            # s2 = r2 + (rm2/tau).sum()
            # u = N * np.log(r2 + (rm2/tau).sum()) + (self.w*np.log(tau)).sum()
            # g = (N/s2) * (rm2*self.nobs/tau**2).sum() + (self.w*self.nobs/tau).sum()
            # return u, g

        if initvar is None:
            res = minimize_scalar(fun, method= 'bounded', bounds = [1e-6, 100])
            mu = res.x
        else:
            x0 = initvar[0]/initvar[1]
            res = minimize(fungrad, np.array(x0), jac=True, method= 'L-BFGS-B', bounds = Bounds(lb=1e-6, ub=100))
            mu = res.x[0]
        # mu = res.x
        tau = 1 + mu * self.nobs
        sigma2 = (r2 + (rm2 / tau).sum())/N
        rho2 = mu*sigma2
        if verb:
            print(f'rho = {np.sqrt(rho2):.4f}, sigma = {np.sqrt(sigma2):.4f}')
        return [rho2, sigma2]

    def fit(self,  Decreasing = True, normalize = False, leftFlat=False, rightFlat = False,
            random_effect = True, single_cp = False,  verb = False, time_lb = 0.,
            delta_lb=1e-6, firstRun = True, max_iter = 100, init_quantile = .3, init_cp = None, change_points = None,):
        mt = self.t.min()
        Mt = self.t.max() - mt
        if normalize:
            self.t = (self.t - mt)/Mt


        if change_points is not None:
            change_points = np.array(change_points)
        with_cp = change_points is None
        with_var = False
        testGrad = False
        if 'cupy' in sys.modules:
            nobs_ = cp.asarray(self.nobs)
            Y_ = cp.asarray(self.Y)
            t_ = cp.asarray(self.t)
            Z_ = cp.asarray(self.Z)
            w_ =  cp.asarray(self.w)
        else:
            nobs_ = self.nobs
            Y_ = self.Y
            t_ = self.t
            Z_ = self.Z
            w_ = self.w

        def fun(x, _variances):
            obj, grad = _objective_and_gradient(nobs_, Y_, t_, Z_, x, w_, variances=_variances,
                                                reff=random_effect, with_gradient=self.withJac, single_cp=single_cp,
                                                change_points = change_points)
            #print(x[4], x[5])
            if testGrad:
                eps = 1e-6
                for j in range(x.shape[0]):
                    delt = np.random.normal(0,1,1)[0]
                    xx = np.copy(x)
                    xx[j] += eps*delt
                    obj2, foo = _objective_and_gradient(self.nobs, self.Y, self.t, self.Z, xx, self.w,
                                                        variances=_variances, reff=random_effect, with_gradient=False,
                                                        single_cp=single_cp, change_points = change_points)
                    print(f'---- Test gradient: var {j} {x[j]:05f} {xx[j]:.5f} {(obj2 - obj)/eps:0.6f}, {(grad[j]*delt):0.6f}')
            if self.withJac:
                return obj, grad
            else:
                return obj


        # order: a, b, c, d, t0, t1, rho, sigma, v
        if firstRun:
            self.fit_linear(reff=random_effect)
            self.linear_likelihood = -self.objective(single_cp=single_cp)
            if Decreasing is None:
                if self.param['b'] < 0:
                    Decreasing = True
                else:
                    Decreasing = False

            if  change_points is None and init_cp is None:
                if np.isscalar(init_quantile):
                    t0 = np.quantile(self.t, init_quantile)
                    t1 = np.quantile(self.t, 1-init_quantile)
                else:
                    t0 = np.quantile(self.t, init_quantile[0])
                    t1 = np.quantile(self.t, init_quantile[1])
            else:
                if change_points is None:
                    t0 = init_cp[0]
                    t1 = init_cp[1]
                else:
                    t0 = change_points[0]
                    t1 = change_points[1]
            self.param.update_time_param(t0, t1)

            if verb:
                self.printParam(message='Linear Model: ')
                print(f'Objective: {self.linear_likelihood:0.4f}')

            r2, s2 = self.fit_variances(reff=random_effect, initvar=(self.param['rho']**2, self.param['sigma']**2))
            self.param['rho'] = np.sqrt(r2)
            self.param['sigma'] = np.sqrt(s2)


        if single_cp:
            nvreg = 3
        else:
            nvreg = 4
        if with_cp:
            nvt = 2
        else:
            nvt = 0

        if random_effect:
            nvvar = 2
        else:
            nvvar = 1

        if self.Z is None:
            nvz = 0
        else:
            nvz = self.Z.shape[1]

        nv0 = nvreg + nvvar + nvt
        nv = nv0 + nvz


        if single_cp:
            nlinear = 1
        else:
            nlinear = 4
        if with_cp and time_lb is not None:
            nlinear += 1

        A = np.zeros((nlinear,nv))

        # b+c
        A[0,1] = 1
        A[0,2] = 1

        if single_cp:
            if with_cp and time_lb is not None:
                # t0 = tm - delta/2
                A[1, 3] = 1
                A[1, 4] = -0.5
        else:
            #b+d
            A[1,1] = 1
            A[1,3] = 1
            #2c + d
            A[2,2] = 2
            A[2,3] = 1
            #c + 2d
            A[3,2] = 1
            A[3,3] = 2
            if with_cp and time_lb is not None:
                # t0 = tm - delta/2
                A[4, 4] = 1
                A[4, 5] = -0.5

        lb = np.zeros(nv)
        ub = np.zeros(nv)
        ub[:] = np.inf
        lb[:] = -np.inf

        if single_cp:
            # b
            if Decreasing:
                lb[1] = -np.inf
                ub[1] = 0
            else:
                lb[1] = 0
                ub[1] = np.inf

            #t1 > t0
        if with_cp:
            lb[nvreg+1] = delta_lb
            ub[nvreg+1] = np.inf

        # positive variances
        lb[nvreg + nvt] = 1e-6
        if random_effect:
            lb[nvreg+nvt+1] = 1e-6

        lbl = np.zeros(nlinear)
        ubl = np.zeros(nlinear)

        if single_cp:
            if Decreasing:
                lbl[0] = -np.inf
                ubl[0] = 0
            else:
                lbl[0] = 0
                ubl[0] = np.inf
            # optional lower bound on t0
            if with_cp and time_lb is not None:
                lbl[1] = time_lb + 1e-6
                ubl[1] = np.inf
        else:
            if Decreasing:
                lbl[:2] = -np.inf
                if leftFlat:
                    lbl[0] = 0
                if rightFlat:
                    lbl[1] = 0
                ubl[:2] = 0
                lbl[2:4] = 0
                ubl[2:4] = np.inf
            else:
                lbl[:2] = 0
                ubl[:2] = np.inf
                if leftFlat:
                    ubl[0] = 0
                if rightFlat:
                    ubl[1] = 0
                lbl[2:4] = -np.inf
                ubl[2:4] = 0

            #optional lower bound on t0
            if with_cp and time_lb is not None:
                lbl[4] = time_lb + 1e-6
                ubl[4] = np.inf

        J = np.ones(nv, dtype=bool)
        if with_var == False:
            J[nvreg+nvt:nv0] = False
        lcons = LinearConstraint(A[:,J], lbl, ubl, keep_feasible=False)

        bds = Bounds(lb[J], ub[J])
        maxiter = 100
        for nit in range(max_iter):
            if verb:
                print(f'Iteration {nit+1}')
            x0 = self.getVariable(reff=random_effect, withVariances=with_var, single_cp = single_cp, with_cp=with_cp)
            obj = self.objective(single_cp = single_cp) # foo = fun(x0, np.array([self.rho, self.sigma]), reff)
            if with_var:
                res = minimize(fun, x0, args = (None,), method=self.method,
                               jac=self.withJac, bounds=bds, options={'maxiter':maxiter}, constraints=lcons)
            else:
                res = minimize(fun, x0, args=(np.array([self.param['rho'], self.param['sigma']]),), method=self.method,
                               jac=self.withJac, bounds=bds, options={'maxiter':maxiter, 'verbose':0}, constraints=lcons)
            if verb:
                print(res.message)
                print(f'Likelihood after parameter estimation {res.fun}')

            if nit > 0 and  res.fun > obj - 1e-6:
                if verb and res.fun > obj:
                    print(f'increased likelihood {res.fun - obj:.7f}')
                if with_var:
                    res = minimize(fun, x0, args=(None, ), method=self.method,
                                   jac=self.withJac, bounds=bds, options={'maxiter': 1000}, constraints=lcons)
                else:
                    res = minimize(fun, x0, args=(np.array([self.param['rho'], self.param['sigma']]), ),
                                   method=self.method,
                                   jac=self.withJac, bounds=bds, options={'maxiter': 1000, 'verbose': 0}, constraints=lcons)
                if verb:
                    print(res.message)
                    print(f'Likelihood after second try of parameter estimation {res.fun}')
                if res.fun < obj:
                    self.setParam_(res.x, withVariances=with_var, reff=random_effect, single_cp=single_cp, with_cp=with_cp)
                break
            self.setParam_(res.x, withVariances=with_var, reff=random_effect, single_cp=single_cp, with_cp=with_cp)
            if verb:
                self.printParam('Fixed variances: ')
            r2, s2 = self.fit_variances(reff=random_effect)
            self.param['rho'] = np.sqrt(r2)
            self.param['sigma'] = np.sqrt(s2)

            if verb:
                print(f'Likelihood after variance estimation = {self.objective(single_cp=single_cp)}')

        self.likelihood = -self.objective(single_cp=single_cp)
        if verb:
            self.printParam('Final result: ')
            print(f'Objective: {self.likelihood:0.4f}')


    def fit_linear(self, reff=True, firstRun = True, max_iter = 100):
        phi1 = np.ones(self.t.shape)
        phi2 = self.t - self.param['tm']
        if self.Z is None:
            X = np.concatenate((phi1, phi2), axis=1)
        else:
            X = np.concatenate((phi1, phi2, self.Z), axis=1)

        sX = np.zeros((self.nsub, X.shape[1]))
        kj = 0
        for k in range(self.w.shape[0]):
            for j in range(self.nobs[k]):
                sX[k, :] += X[kj,:]
                kj += 1

        wX = np.zeros(X.shape)
        oldrho = self.param['rho']
        oldsigma = self.param['sigma']
        tol = 0.0001

        for ni in range(max_iter):
            mu2 = (self.param['rho'] / self.param['sigma']) ** 2
            tau2 = mu2 / (1 + self.nobs * mu2)
            kj = 0
            for k in range(self.w.shape[0]):
                for j in range(self.nobs[k]):
                    wX[kj, :] = self.w[k] * (X[kj,:] - tau2[k] * sX[k])
                    kj += 1
            beta = LA.solve(wX.T@X, wX.T@self.Y)[:,0]
            self.param['a'] = beta[0]
            self.param['b'] = beta[1]
            if self.Z is not None:
                self.param['v'] = beta[2:]
            self.param['c'] = 0
            self.param['d'] = 0
            rho2, sigma2 = self.fit_variances(reff=reff)
            if reff:
                self.param['rho'] = np.sqrt(rho2)
            else:
                self.param['rho'] = 0
            self.param['sigma'] = np.sqrt(sigma2)
            if np.fabs(self.param['rho'] - oldrho) < tol and np.fabs(self.param['sigma'] - oldsigma) < tol:
                break
            else:
                oldsigma = self.param['sigma']
                oldrho = self.param['rho']

        if firstRun:
            t0 = np.quantile(self.t, 0.55)
            t1 = np.quantile(self.t, 0.65)
            self.param.update_time_param(t0, t1)
            # self.param['tm'] = (self.param['t0']+self.param['t1'])/2
            # self.param['delta'] = self.param['t1'] - self.param['t0']


        # plt.figure('Linear')
        #self.plot()

    # def fit_full_linear(self, id=None, Y=None, t=None, Z=None, reff=True):
    #     if id is not None:
    #         self.id = id
    #     if Y is not None:
    #         self.Y = Y
    #     if t is not None:
    #         self.t = t
    #     if Z is not None:
    #         self.Z = Z
    #     self.t0 = np.quantile(self.t, 0.25)
    #     self.t1 = np.quantile(self.t, 0.75)
    #     self.tm = (self.t0+self.t1)/2
    #     self.delta = self.t1 - self.t0
    #
    #     before = self.t < self.t0
    #     after = self.t > self.t1
    #     during = np.logical_and(self.t >= self.t0, self.t <= self.t1)
    #     tbefore = self.t[before]
    #     tduring = self.t[during]
    #     phi1 = np.ones(self.t.shape)
    #     phi2 = self.delta * (self.t - self.tm)
    #     phi3 = np.zeros(self.t.shape)
    #     phi3[before] = self.delta ** 2
    #     phi3[during] = (tduring - self.t1) ** 2
    #     phi3 *= (self.t - self.t0) / self.delta
    #     phi4 = np.zeros(t.shape)
    #     phi4[during] = (tduring - self.t0) ** 2
    #     phi4[after] = self.delta ** 2
    #     phi3 *= (t - self.t1) / self.delta
    #
    #     uniqueId = np.unique(self.id)
    #     nsub = uniqueId.shape[0]
    #     nobs = np.zeros(nsub, dtype=int)
    #     for k in range(nsub):
    #         nobs[k] = (self.id == uniqueId[k]).sum()
    #     # order: a, b, c, d, t0, t1, rho, sigma, v
    #
    #     X = np.concatenate((phi1[:, None], phi2[:, None], phi3[:, None], phi4[:, None]), axis=1)
    #     beta = LA.solve(X.T@X, X.T@self.Y)
    #     self.a = beta[0]
    #     self.b = beta[1]
    #     self.c = beta[2]
    #     self.d = beta[3]
    #     res = self.Y - X@beta
    #     if reff:
    #         resm = np.zeros(nsub)
    #         kj = 0
    #         for k in range(nsub):
    #             for j in range(nobs[k]):
    #                 resm[k] += res[kj]
    #                 kj += 1
    #         resm /= nobs
    #         self.rho = np.sqrt((resm**2).mean())
    #         kj = 0
    #         for k in range(nsub):
    #             for j in range(nobs[k]):
    #                 res[kj] -= resm[k]
    #                 kj += 1
    #     self.sigma = np.sqrt((res**2).mean())
    #     plt.figure('full linear')
    #     self.plot()
    #     plt.show()