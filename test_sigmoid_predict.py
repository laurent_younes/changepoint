import numpy as np
import pandas as pd
from sigmoid import SigmoidalRegression, sigFunction
import matplotlib
matplotlib.use("QT5Agg")
import matplotlib.pyplot as plt
plt.ion()
generate_data = True
decreasing = None
leftFlat = False
time_lb = 100
nboot_t0 = 100
nboot_lin = 100
plot = True
verb = True


DATA_DIR = '/Users/younes/OneDrive - Johns Hopkins University/RESEARCH/Projects/PREDICT/2020/'

df = pd.read_csv(DATA_DIR + 'trace_predict_cpa1.csv')
#df = pd.read_csv(DATA_DIR + 'AD_predict_cpa.csv')
#df = pd.read_csv(DATA_DIR + 'FA_predict_cpa.csv')
id = df['id'].to_numpy(dtype=int)
#structures = df.columns[:-2]
structures=('PUTAMEN_Y',)

for s in structures:
    print(s)
    #Y = df['PUTAMEN_Y'].to_numpy()
    Y = df[s].to_numpy()
    t = df['t'].to_numpy()

    #weights = np.random.exponential(1, nsub)
    S = SigmoidalRegression(id, Y, t)
    S.fit(random_effect=True, normalize=False, Decreasing=decreasing, leftFlat=leftFlat, verb=verb, time_lb=time_lb)
    S.printParam()
    print(f'Likelihood: {S.likelihood}')
    print(f'Likelihood difference: {-S.linear_likelihood + S.likelihood: .4f}')
    if plot:
        S.plot(figure=s+' Full model')
    print('\n')
    #plt.ioff()
    #plt.pause(1)



    # Sw = SigmoidalRegressionWithWeights(id, Y, t, Z=Z)
    # Sw.fit(random_effect=True, normalize=False, Decreasing=decreasing, leftFlat=False, verb=True)
    # Sw.printParam()
    # print(f'Likelihood: {Sw.likelihood}')
    # print(f'Likelihood difference: {Sw.linear_likelihood - Sw.likelihood: .4f}')
    lr = -S.linear_likelihood + S.likelihood

    #Compute conf interval for t0
    nboot = nboot_t0
    boott0 = np.zeros(nboot)
    for k in range(nboot):
        #generate a sample using the linear model
        idb, tb, Yb = S.generate_bootstrap()
        Sb = SigmoidalRegression(idb, Yb, tb)
        #fit the nonlinear model
        Sb.fit(random_effect=True, normalize=False, Decreasing=decreasing, verb=False, leftFlat=leftFlat, time_lb=time_lb)
        if verb:
            print(f'Bootstrap {k} t0: {Sb.param.t0: .4f}')
        #Compute likelihood difference
        boott0[k] = Sb.param.t0
        if plt:
            Sb.plot(figure=f'Boostrap t0')
        #plt.ioff()
        #plt.pause(1)

    if nboot > 0:
        sigmat0 = np.std(boott0)
        print(f'Std of t0: {sigmat0:.4f}')


    #Test for non-linearity
    #Train linear model
    Sl = SigmoidalRegression(id, Y, t)
    Sl.fit_linear(reff=True)

    #Number of bootstrap samples
    nboot = nboot_lin
    bootlr = np.zeros(nboot)
    for k in range(nboot):
        #generate a sample using the linear model
        idb, tb, Yb = Sl.generate_bootstrap()
        Sb = SigmoidalRegression(idb, Yb, tb)
        #fit the nonlinear model
        Sb.fit(random_effect=True, normalize=False, Decreasing=decreasing, verb=False, leftFlat=leftFlat, time_lb=time_lb)
        if plot:
            Sb.plot(figure=f'Boostrap linear')
        #plt.ioff()
        #plt.pause(1)
        #Compute likelihood difference
        bootlr[k] = -Sb.linear_likelihood + Sb.likelihood
        if verb:
            print(f'Bootstrap {k} Likelihood difference: {-Sb.linear_likelihood + Sb.likelihood: .4f}')

    if nboot > 0:
        pvalue = np.mean(bootlr>=lr)
        print(f'p-value: {pvalue:.5f}')

