import numpy as np
from scipy.stats import norm
from scipy.optimize import minimize, Bounds





def estimate(t, tmin, tmax, censored):
    n = t.shape[0]
    J0 = np.nonzero(censored==0)[0]
    J1 = np.nonzero(censored)[0]
    n1 = censored.sum()
    n0 = n - n1
    def fun(x):
        c = 0.5 * np.log(2*np.pi)
        m = x[0]
        sigma = x[1]
        t_ = (t-m)/sigma
        tmin_ = (tmin - m)/sigma
        tmax_ = (tmax - m)/sigma
        s2 = (t_[J0]**2).sum()
        s1 = t_[J0].sum()
        # obj = - n0 * np.log(2*np.pi*sigma) - 0.5 * s2
        # umax = np.zeros(n)
        # umin = np.zeros(n)
        # for k in J1:
        #     umax[k] = np.maximum(norm.sf(tmax_[k]), 1e-100)
        # for k in range(n):
        #     umin[k] = np.maximum(norm.sf(tmin_[k]), 1e-10)
        logsftmax = norm.logsf(tmax_)
        logsftmin = norm.logsf(tmin_)
        obj = - n0 * c - n0 * np.log(sigma) - 0.5 * s2 + logsftmax[J1].sum() #- logsftmin.sum()
        # for k in J1:
        #     #obj += np.log(umax[k]) - np.log(umin[k])
        #     obj += norm.logsf(tmax_[k]) - norm.logsf(tmin_[k])
        # for k in J0:
        #     # obj -= np.log(umin[k])
        #     obj -= norm.logsf(tmin_[k])
        # gmax = 0
        # gmin = 0
        # smax = 0
        # smin = 0
        gmax = np.exp(-np.minimum(0.5*tmax_[J1]**2 + c + logsftmax[J1], 100)).sum()
        smax = (tmax_[J1]*np.exp(-np.minimum(0.5*tmax_[J1]**2 + c + logsftmax[J1], 100))).sum()
        gmin = np.exp(-np.minimum(0.5*tmin_**2 + c + logsftmin, 100)).sum()
        smin = (tmin_*np.exp(-np.minimum(0.5*tmin_**2 + c + logsftmin, 100))).sum()
        # for k in J1:
        #     u = np.exp(-min(0.5*tmax_[k]**2 + c + logsftmax[k], 100))
        #     #u = norm.pdf(tmax_[k]) / umax[k]
        #     gmax += u
        #     smax += tmax_[k]*u
        #     u = np.exp(-min(0.5*tmin_[k]**2 + c + norm.logsf(tmin_[k]), 100))
        #     # u = norm.pdf(tmin_[k]) / umin[k]
        #     gmin += u
        #     smin += tmin_[k] * u
        # for k in J0:
        #     # u = norm.pdf(tmin_[k]) / umin[k]
        #     u = np.exp(-min(0.5*tmin_[k]**2 + c + norm.logsf(tmin_[k]), 100))
        #     gmin += u
        #     smin += tmin_[k] * u

        grad = np.zeros(2)
        grad[0] = s1 / sigma + gmax/sigma #- gmin/sigma
        grad[1] = - n0 /sigma + s2 / sigma + smax/sigma #-smin/sigma

        return -obj, -grad


    x0 = np.array([0,1])
    #bds = Bounds([t.min(), 1e-6], [t.max(), np.inf])
    bds = Bounds([-np.inf, 1e-6], [np.inf, np.inf])
    res = minimize(fun, x0, method='L-BFGS-B', jac=True, bounds=bds, options={'maxiter': 1000})

    return res.x[0], res.x[1]