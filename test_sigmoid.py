import numpy as np
import pandas as pd
from sigmoid import SigmoidalRegression, sigFunction
import matplotlib
matplotlib.use("QT5Agg")
import matplotlib.pyplot as plt
plt.ion()
generate_data = True
decreasing = True

if generate_data:
    nsub = 1000
    npersub = 5
    ncov = 1
    nobs = npersub * np.ones(nsub, dtype=int)
    N = nsub*npersub
    t0 = -10
    t1 = 10
    a = -200
    if decreasing:
        c = 0.5
        d = 0.
        # c=0
        # d=0
        b = -0.6
    else:
        c = -0.025
        d = -0.025
        # c=0
        # d=0
        b = 0.03
    rho = 10
    sigma = 5
    v = 0
    res = f'True param: a={a:.4f} b={b:.4f} c={c:.4f} d={d:.4f} t0={t0:.4f} t1={t1:.4f} '
    res += f'rho={rho:.4f} sigma={sigma:.4f}'
    res += f' v={v:.4f} '
    print(res)

    t = np.zeros(N)
    Z = np.zeros((N,ncov))
    kj = 0
    tmin = -50
    tmax = 50
    noise = sigma * np.random.normal(0, 1, (N,1))
    id = np.zeros(N, dtype=int)
    for k in range(nsub):
        reff = rho*np.random.normal(0,1,1)
        t[kj] = tmin + np.random.rand(1)[0] * (tmax-tmin)
        noise[kj] += reff
        id[kj] = k
        kj += 1
        for j in range(1, npersub):
            t[kj] = t[kj-1] + 2
            noise[kj] += reff
            id[kj] = k
            if k < nsub/2:
                Z[kj] = 1
            kj += 1


    Y = sigFunction(t, a, b, c, d, (t0+t1)/2, t1-t0)[:, None]  + np.dot(Z,v) + noise
    data = dict()
    data['t'] = t
    data['id'] = id
    data['Y'] = Y[:,0]
    for j in range(ncov):
        data[f'Z{j+1}'] = Z[:,j]
    df = pd.DataFrame(data)
    df.to_csv('data2.csv')
    #plt.plot(t, Y, '*')
else:
    df = pd.read_csv('data2.csv')
    id = df['id'].to_numpy(dtype=int)
    t = df['t'].to_numpy()[:, None]
    Y = df['Y'].to_numpy()[:, None]
    ncov = len(df.keys()) - 4
    N = Y.shape[0]
    Z = np.zeros((N,ncov))
    for i in range(ncov):
        Z[:,i] = df[f'Z{i+1}'].to_numpy()
    uid = np.unique(id)
    nsub = uid.shape[0]
    nobs = np.zeros(nsub, dtype=int)
    for k in range(nsub):
        nobs[k] = np.array(id == uid[k]).sum()



# Train full model
testWeights = True

if testWeights:
    newid = np.zeros(2*N, dtype=int)
    newY = np.zeros((2*N, 1))
    newt = np.zeros(2*N)
    newZ = np.zeros((2*N, Z.shape[1]))
    weights = 0.5 * np.ones(2*nsub)
    kj = 0
    nkj = 0
    for k in range(nsub):
        newid[nkj:nkj+nobs[k]] = 2*k
        newY[nkj:nkj+nobs[k], :] = Y[kj:kj+nobs[k], :]
        newt[nkj:nkj+nobs[k]] = t[kj:kj+nobs[k]]
        newZ[nkj:nkj+nobs[k], :] = Z[kj:kj+nobs[k], :]
        nkj += nobs[k]
        newid[nkj:nkj+nobs[k]] = 2*k+1
        newY[nkj:nkj+nobs[k], :] = Y[kj:kj+nobs[k], :]
        newt[nkj:nkj+nobs[k]] = t[kj:kj+nobs[k]]
        newZ[nkj:nkj+nobs[k], :] = Z[kj:kj+nobs[k], :]
        nkj += nobs[k]
        kj += nobs[k]
else:
    newid = id
    newY = Y
    newt = t
    newZ = Z
    weights = None
#weights = np.random.exponential(1, nsub)
init_quantile = (0.3, 0.7)
S = SigmoidalRegression(newid, newY, newt, Z=newZ, w=weights)
S.fit(random_effect=True, normalize=False, Decreasing=decreasing, leftFlat=False, verb=True,
      init_quantile=init_quantile, time_lb=None, single_cp=True)
S.printParam()
print(f'Likelihood: {S.likelihood}')
print(f'Likelihood difference: {-S.linear_likelihood + S.likelihood: .4f}')
S.plot(figure = 'Full model')
# plt.ioff()
# plt.show()



# Sw = SigmoidalRegressionWithWeights(id, Y, t, Z=Z)
# Sw.fit(random_effect=True, normalize=False, Decreasing=decreasing, leftFlat=False, verb=True)
# Sw.printParam()
# print(f'Likelihood: {Sw.likelihood}')
# print(f'Likelihood difference: {Sw.linear_likelihood - Sw.likelihood: .4f}')
lr = - S.linear_likelihood + S.likelihood

#Compute conf interval for t0
nboot = 100
boott0 = np.zeros(nboot)
for k in range(nboot):
    #generate a sample using the linear model
    idb, tb, Yb, Zb = S.generate_bootstrap()
    Sb = SigmoidalRegression(idb, Yb, tb, Z=Zb)
    #fit the nonlinear model
    Sb.fit(random_effect=True, normalize=False, Decreasing=decreasing, verb=False, leftFlat=False, init_quantile=init_quantile)
    print(f"Bootstrap {k} t0: {(Sb.param['t0']+Sb.param['t1'])/2: .4f}")
    #Compute likelihood difference
    boott0[k] = (Sb.param['t0'] + Sb.param['t1'])/2

if nboot > 0:
    sigmat0 = np.std(boott0)
    print(f'Std of t0: {sigmat0:.4f}')


#Test for non-linearity
#Train linear model
Sl = SigmoidalRegression(id, Y, t, Z=Z)
Sl.fit_linear(reff=True)

#Number of bootstrap samples
bootlr = np.zeros(nboot)
for k in range(nboot):
    #generate a sample using the linear model
    idb, tb, Yb, Zb = Sl.generate_bootstrap()
    Sb = SigmoidalRegression(idb, Yb, tb, Z=Zb)
    #fit the nonlinear model
    Sb.fit(random_effect=True, normalize=False, Decreasing=decreasing, verb=False, leftFlat=False, init_quantile=init_quantile)
    #Compute likelihood difference
    bootlr[k] = - Sb.linear_likelihood + Sb.likelihood
    print(f'Bootstrap {k} Likelihood difference: {-Sb.linear_likelihood + Sb.likelihood: .4f}')

if nboot > 0:
    pvalue = np.mean(bootlr>=lr)
    print(f'p-value: {pvalue:.5f}')

#plt.figure('Full model')
S.plot(figure='Full model')
plt.ioff()
plt.show()
