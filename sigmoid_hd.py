#%% raw
#import predict_pca
import sys
import numpy as np
import pandas as pd
import pickle
import matplotlib
matplotlib.use("QT5Agg")
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
#import seaborn as sns
# # from pylab import *
# from scipy.optimize import curve_fit
# from scipy.optimize import minimize
# import random
# import csv

# from scipy.optimize import LinearConstraint, Bounds, NonlinearConstraint, BFGS
from HD_bin import remove_Subj_less3, get_Track_vol_features, get_Predict_vol_features, get_Track_control_vol_features
from HD_bin import get_ImageHD_vol_features, get_ImageHD_control_vol_features, get_Predict_control_vol_features
from HD_bin import aligned_controls_cap, plot_Track_control_vols, plot_Predict_control_vols, plot_ImageHD_control_vols
from HD_bin import plot_Predict_vols, plot_ImageHD_vols, plot_Track_vols
from sigmoid import SigmoidalRegression

SMALL_SIZE = 26
MEDIUM_SIZE = 30
BIGGER_SIZE = 36

plt.rc('font', size=SMALL_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=16)  # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

#%% raw
# Load data
#%% raw
Date = 'May21'
#%% raw
# features = ['Put']
# feature_full = 'Putamen'

features = ['Put']
feature_full = 'Caudate'

# features = ['GP']
# feature_full = 'Globus Pallidus'

# features = ['NucAccumbens']
# feature_full = 'Nucleus Accumbens'

# features = ['Snigra']
# feature_full = 'Substantia Nigra'

# features = ['Thalamus']
# feature_full = 'Thalamus'

# features = ['PrCG']
# feature_full = 'PrCG'

# features = ['PrCWM']
# feature_full = 'PrCWM'

# features = ['PoCG']
# feature_full = 'PoCG'

# features = ['PoCWM']
# feature_full = 'PoCWM'


# features = ['SFG']
# feature_full = 'SFG'

# features = ['STG']
# feature_full = 'STG'

# features = ['SOG']
# feature_full = 'SOG'

# features = ['SPG']
# feature_full = 'SPG'


# features = ['SFWM']
# feature_full = 'SFWM'

# features = ['STWM']
# feature_full = 'STWM'

# features = ['SOWM']
# feature_full = 'SOWM'

# features = ['SPWM']
# feature_full = 'SPWM'

# ---------------------------------

# features = ['Amyg']
# feature_full = 'Amygdala'

# features = ['Hippo']
# feature_full = 'Hippocampus'

# features = ['CerebellumGM', 'CerebellumWM','MCP_cb']
# feature_full = 'Cerebellum'


# -----------------------------------------
# features = ['HypoThalamus']
# feature_full = 'HypoThalamus'

# features = ['Pons']
# feature_full = 'Pons'

# features = ['RedNc']
# feature_full = 'RedNc'

# features = ['Insula']
# feature_full = 'Insula'

# features = ['Midbrain']
# feature_full = 'Midbrain'
#%% raw
# with open('df_voldemo_qc_imhd_atleast1.pickle', "rb") as inputfile:
#     imhd_df = pickle.load(inputfile)
# with open('df_voldemo_qc_THD_atleast1.pickle', "rb") as inputfile:
#     thd_df = pickle.load(inputfile)
# with open('df_voldemo_qc_TON_atleast1.pickle', "rb") as inputfile:
#     ton_df = pickle.load(inputfile)
# with open('df_voldemo_qc_predict_atleast1.pickle', "rb") as inputfile:
#     phd_df = pickle.load(inputfile)

# phd_df['subjid'] = phd_df['subject_id'].apply(lambda x: x.split('_')[0])
# thd_df['cap'] = thd_df['Age at Scan'] * (thd_df['CagLarge'] - 33.66)
# imhd_df['cap'] = (imhd_df['CAG repeats'] - 33.66) * imhd_df['Age']

datadir = '/Users/younes/OneDrive - Johns Hopkins University/RESEARCH/Projects/Predict/2020/fromChinFu/'
with open(datadir + 'df_voldemo_qc_imhd_atleast3.pickle', "rb") as inputfile:
    imhd_df = pickle.load(inputfile)
with open(datadir + 'df_voldemo_qc_THD_atleast3.pickle', "rb") as inputfile:
    thd_df = pickle.load(inputfile)
with open(datadir + 'df_voldemo_qc_TON_atleast3.pickle', "rb") as inputfile:
    ton_df = pickle.load(inputfile)
with open(datadir + 'df_voldemo_qc_predict_atleast3.pickle', "rb") as inputfile:
    phd_df = pickle.load(inputfile)

phd_df['subjid'] = phd_df['subject_id'].apply(lambda x: x.split('_')[0])
thd_df['cap'] = thd_df['Age at Scan'] * (thd_df['CagLarge'] - 33.66)
imhd_df['cap'] = (imhd_df['CAG repeats'] - 33.66) * imhd_df['Age']

thd_df = remove_Subj_less3(thd_df, 'Subject ID')
phd_df = remove_Subj_less3(phd_df, 'subjid')
imhd_df = remove_Subj_less3(imhd_df, 'ID')

#%% raw
# retrospect QC this subject has failure segmentation due to artifact
phd_df = phd_df[phd_df['subjid'] != 'sub-575592']
#%% raw
# imhd_df = imhd_df[imhd_df['minor issue']!='cerebellum oversegmentation']
# thd_df = thd_df[thd_df['minor issue']!='cerebellum oversegmentation']
# ton_df = ton_df[ton_df['minor issue']!='cerebellum oversegmentation']
# phd_df = phd_df[phd_df['minor issue']!='cerebellum oversegmentation']

# features = ['CerebellumGM', 'CerebellumWM','MCP_cb']
# # feature_full = 'Cerebellum'

# def exclude_extreme(df, features=['Put'], UPmargin=None, LWmargin=None):
#     tmp = 0
#     for feature in features:
#         tmp = tmp+(df[feature + '_L'] + df[feature + '_L'])/2
#     if UPmargin is not None:
#         df = df[tmp<UPmargin]
#     if LWmargin is not None:
#         df = df[tmp>LWmargin]
#     return df

# UPmargin = 120000
# imhd_df = exclude_extreme(imhd_df, features=features, UPmargin=UPmargin)
# thd_df = exclude_extreme(thd_df, features=features, UPmargin=UPmargin)
# ton_df = exclude_extreme(ton_df, features=features, UPmargin=UPmargin)
# phd_df = exclude_extreme(phd_df, features=features, UPmargin=UPmargin)
#%% raw
# imhd_df = imhd_df[imhd_df['segmentation status,                good = 0, fail = 1']!=1]
# thd_df = thd_df[thd_df['segmentation status,                good = 0, fail = 1']!=1]
# ton_df = ton_df[ton_df['segmentation status,                good = 0, fail = 1']!=1]
# phd_df = phd_df[phd_df['segmentation status,                good = 0, fail = 1']!=1 ]
#%% raw
## get whole brain features
Table_path = datadir + 'multilevel_lookup_table.txt'
df = pd.read_csv(Table_path, sep="\t", header=None)
df = df.drop(columns=[0])
df = df.drop([0])

df = df[df.iloc[:, 4] != 'Myelencephalon']
df = df[df.iloc[:, 4] != 'Metencephalon']
df = df.dropna()
IC_features = df.iloc[:, 0].to_list()

df = df[df.iloc[:, 9] != 'CSF']
df = df.dropna()
#%% raw
for optt in [0]:
    t0_vis = 150
    t1_vis = 600
    if optt == 0:
        WB_Norm_features = df.iloc[:, 0].to_list()
        WB_Norm = True
        age_margin = 40

    elif optt == 1:
        WB_Norm_features = df.iloc[:, 0].to_list()
        WB_Norm = True
        age_margin = 0

    elif optt == 2:
        WB_Norm_features = None
        WB_Norm = False
        age_margin = 0

    elif optt == 3:
        WB_Norm_features = None
        WB_Norm = False
        age_margin = 40

        ## load data
    X_t, y_t, gp_t, wb_t, ic_t = get_Track_vol_features(thd_df, features, feature_full,
                                                        WB_Norm_features=WB_Norm_features, WB_Norm=WB_Norm,
                                                        IC_features=IC_features)
    X_t_cont, y_t_cont, gp_t_cont, wb_t_cont, ic_t_cont = get_Track_control_vol_features(thd_df, features, feature_full,
                                                                                         age_margin=age_margin,
                                                                                         WB_Norm_features=WB_Norm_features,
                                                                                         WB_Norm=WB_Norm,
                                                                                         IC_features=IC_features)

    X_p, y_p, gp_p, wb_p, ic_p = get_Predict_vol_features(phd_df, features, feature_full,
                                                          WB_Norm_features=WB_Norm_features, WB_Norm=WB_Norm,
                                                          IC_features=IC_features)
    X_p_cont, y_p_cont, gp_p_cont, wb_p_cont, ic_p_cont = get_Predict_control_vol_features(phd_df, features,
                                                                                           feature_full,
                                                                                           age_margin=age_margin,
                                                                                           WB_Norm_features=WB_Norm_features,
                                                                                           WB_Norm=WB_Norm,
                                                                                           IC_features=IC_features)

    X_i, y_i, gp_i, wb_i, ic_i = get_ImageHD_vol_features(imhd_df, features, feature_full,
                                                          WB_Norm_features=WB_Norm_features, WB_Norm=WB_Norm,
                                                          IC_features=IC_features)
    X_i_cont, y_i_cont, gp_i_cont, wb_i_cont, ic_i_cont = get_ImageHD_control_vol_features(imhd_df, features,
                                                                                           feature_full,
                                                                                           age_margin=age_margin,
                                                                                           WB_Norm_features=WB_Norm_features,
                                                                                           WB_Norm=WB_Norm,
                                                                                           IC_features=IC_features)

    X_t_cont = aligned_controls_cap(X_t_cont)
    X_p_cont = aligned_controls_cap(X_p_cont)
    X_i_cont = aligned_controls_cap(X_i_cont)

    y = y_t + y_p + y_i + y_t_cont + y_p_cont + y_i_cont
    t = X_t + X_p + X_i + X_t_cont + X_p_cont + X_i_cont

    z_dataGrp = [[0]] * len(y_t) + [[1]] * len(y_p) + [[2]] * len(y_i) + [[0]] * len(y_t_cont) + [[1]] * len(
        y_p_cont) + [[2]] * len(y_i_cont)
    z_gpt = [[_] for _ in gp_t]
    z_gptc = [[_] for _ in gp_t_cont]

    z_gpp = [[1] if _ == 'low' else [2] if _ == 'med' else [3] for _ in gp_p]
    z_gppc = [[0] if _ == 'cont' else [-1] for _ in gp_p_cont]

    z_gpi = [[float(_)] for _ in gp_i]
    z_gpic = [[float(_)] for _ in gp_i_cont]

    z_subjGrp = z_gpt + z_gpp + z_gpi + z_gptc + z_gppc + z_gpic

    z_ict = [[np.mean(_)] for _ in ic_t]
    z_ict = (z_ict / np.mean(z_ict)).tolist()

    z_ictc = [[np.mean(_)] for _ in ic_t_cont]
    z_ictc = (z_ictc / np.mean(z_ictc)).tolist()

    z_icp = [[np.mean(_)] for _ in ic_p]
    z_icp = (z_icp / np.mean(z_icp)).tolist()

    z_icpc = [[np.mean(_)] for _ in ic_p_cont]
    z_icpc = (z_icpc / np.mean(z_icpc)).tolist()

    z_ici = [[np.mean(_)] for _ in ic_i]
    z_ici = (z_ici / np.mean(z_ici)).tolist()

    z_icic = [[np.mean(_)] for _ in ic_i_cont]
    z_icic = (z_icic / np.mean(z_icic)).tolist()

    z_subjIC = z_ict + z_icp + z_ici + z_ictc + z_icpc + z_icic
    z = np.append(z_dataGrp, z_subjGrp, axis=1)

    if WB_Norm is False:
        z = np.append(z, z_subjIC, axis=1)

    if WB_Norm_features is not None:
        y_mean = np.mean([np.mean(_) for _ in y])
        scale_factor = 1 / y_mean

        y = [[y[i][j] * scale_factor for j in range(len(y[i]))] for i in range(len(y))]
        y_t = [[y_t[i][j] * scale_factor for j in range(len(y_t[i]))] for i in range(len(y_t))]
        y_p = [[y_p[i][j] * scale_factor for j in range(len(y_p[i]))] for i in range(len(y_p))]
        y_i = [[y_i[i][j] * scale_factor for j in range(len(y_i[i]))] for i in range(len(y_i))]
        y_t_cont = [[y_t_cont[i][j] * scale_factor for j in range(len(y_t_cont[i]))] for i in range(len(y_t_cont))]
        y_p_cont = [[y_p_cont[i][j] * scale_factor for j in range(len(y_p_cont[i]))] for i in range(len(y_p_cont))]
        y_i_cont = [[y_i_cont[i][j] * scale_factor for j in range(len(y_i_cont[i]))] for i in range(len(y_i_cont))]

    v = [1] * len(z[0])

    plt.figure(figsize=(15, 10))

    p1 = plot_Track_control_vols(X_t_cont, y_t_cont)
    p2, p3 = plot_Track_vols(X_t, y_t, gp_t)

    p4 = plot_Predict_control_vols(X_p_cont, y_p_cont)
    p5, p6 = plot_Predict_vols(X_p, y_p, gp_p)

    p7 = plot_ImageHD_control_vols(X_i_cont, y_i_cont)
    p8, p9 = plot_ImageHD_vols(X_i, y_i, gp_i)

    if WB_Norm:
        plt.title("%s normalized volume versus CAP Score" % (feature_full))
        plt.xlabel("CAP Score")
        plt.ylabel("%s volume" % (feature_full))
    else:
        plt.title("%s volume versus CAP Score" % (feature_full))
        plt.xlabel("CAP Score")
        plt.ylabel("%s volume" % (feature_full))
    plt.legend(handles=[p1, p2, p3, p4, p5, p6, p7, p8, p9])
    #plt.show()

    nsub = len(t)
    nobs = 0
    for l in t:
        nobs += len(l)

    id = np.zeros(nobs, dtype=int)
    ynp = np.zeros(nobs)
    tnp = np.zeros(nobs)

    j = 0
    for k,l in enumerate(t):
        for kk,t_ in enumerate(l):
            tnp[j] = t_
            id[j] = k
            ynp[j] = y[k][kk]
            j += 1
    znp = z[id, :]

    time_lb = 100
    nboot_t0 = 100
    nboot_lin = 100
    decreasing = True
    leftFlat = True
    verb = True
    plot = True
    S = SigmoidalRegression(id, ynp, tnp, Z=znp)
    S.fit(random_effect=True, normalize=False, Decreasing=decreasing, leftFlat=leftFlat, verb=False, time_lb=time_lb, max_iter=100)
    S.printParam()
    S.plot()
    print(f'Likelihood: {S.likelihood}')
    print(f'Likelihood difference: {-S.linear_likelihood + S.likelihood: .4f}')

    lr = -S.linear_likelihood + S.likelihood

    #Compute conf interval for t0
    nboot = nboot_t0
    boott0 = np.zeros(nboot)
    for k in range(nboot):
        #generate a sample using the linear model
        idb, tb, Yb, Zb = S.generate_bootstrap()
        Sb = SigmoidalRegression(idb, Yb, tb, Z=Zb)
        #fit the nonlinear model
        Sb.fit(random_effect=True, normalize=False, Decreasing=decreasing, verb=False, leftFlat=leftFlat, time_lb=time_lb)
        if verb:
            print(f'Bootstrap {k} t0: {Sb.param.t0: .4f}')
        #Compute likelihood difference
        boott0[k] = Sb.param.t0
        if plt:
            Sb.plot(figure=f'Boostrap t0')
        #plt.ioff()
        #plt.pause(1)

    if nboot > 0:
        sigmat0 = np.std(boott0)
        print(f'Std of t0: {sigmat0:.4f}')


    #Test for non-linearity
    #Train linear model
    Sl = SigmoidalRegression(id, ynp, tnp, Z=znp)
    Sl.fit_linear(reff=True)

    #Number of bootstrap samples
    nboot = nboot_lin
    bootlr = np.zeros(nboot)
    for k in range(nboot):
        #generate a sample using the linear model
        idb, tb, Yb, Zb = Sl.generate_bootstrap()
        Sb = SigmoidalRegression(idb, Yb, tb, Z=Zb)
        #fit the nonlinear model
        Sb.fit(random_effect=True, normalize=False, Decreasing=decreasing, verb=False, leftFlat=leftFlat, time_lb=time_lb)
        if plot:
            Sb.plot(figure=f'Boostrap linear')
        #plt.ioff()
        #plt.pause(1)
        #Compute likelihood difference
        bootlr[k] = -Sb.linear_likelihood + Sb.likelihood
        if verb:
            print(f'Bootstrap {k} Likelihood difference: {-Sb.linear_likelihood + Sb.likelihood: .4f}')

    if nboot > 0:
        pvalue = np.mean(bootlr>=lr)
        print(f'p-value: {pvalue:.5f}')

