import scipy.io as sio
import numpy as np
import time

from covarOnsetwithAD import covarOnsetwithAD

starttime1 = time.time()
input_v1 = sio.loadmat('input_v1.mat')

Y = input_v1['Y']
Z = input_v1['Z']
s = input_v1['s']
t = input_v1['t']
t0 = input_v1['t0']
t1 = input_v1['t1']
b = input_v1['b']
dx = input_v1['dx']

N = Z.shape[0]

ZJ = np.array([True] * N)

n = Z.shape[1]

data = {}

data['svar'] = np.expand_dims(np.log(Y[ZJ, 13]), axis=1)

kn = Z.dot(np.arange(n).T)

data['kn'] = kn[ZJ]
data['gdr'] = s[ZJ]
data['age'] = t[ZJ]

onset = t1[data['kn']]

endAge = np.zeros([n, 1])

for k in range(n):
    endAge[k] = max(t[kn == k])

endAge = endAge[data['kn']]

opt = {}

opt['plotLikelihood'] = True
opt['maxfcall'] = 1000
opt['refinedSearch'] = True
opt['theta'] = .25
opt['cDelta'] = 40
opt['noChangePoint'] = False
opt['noOnset'] = False
opt['normalChangepoint'] = False

m1 = 93
sigma1 = 14.5
lambda1 = 0.165
opt['theta'] = 10

age = np.maximum(data['age'], opt['cDelta'])

opt['paramOnset'] = [m1, sigma1, lambda1]

opt['constrainb1'] = -1
opt['covariates'] = np.zeros((1,), dtype=np.object)
opt['covariates'][0] = data['gdr']
opt['theta'] = 10

opt['fastsearch'] = False

stats = covarOnsetwithAD(data['svar'], data['kn'], dx, age, onset, endAge, opt)

runtime = time.time() - starttime1
sio.savemat('BiomarkerPythonResult.mat', {'stats': stats, 'runtime': runtime})
