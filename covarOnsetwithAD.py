import numpy as np
import scipy as sp
import scipy.optimize
import util
import time
from copy import deepcopy as copy
from matplotlib import pyplot as plt


def covarOnsetwithAD(Y, lab, grp, t, onsetTime, *args):
    # Timestemp
    tcp = time.time()

    if len(args) == 0:
        opt = dict()
    else:
        endTime = args[0]
        opt = args[1]

    if 'noChangePoint' in opt and opt['noChangePoint']:
        # No function called covarOnsetNoChangepoint.
        # stats = covarOnsetNoChangepoint(Y, lab, t, onsetTime, endTime, opt)
        stats = 0
        return stats

    step = opt.get('step', .25)
    cDelta = opt.get('cDelta', 20)
    constrainb1 = opt.get('constrainb1', 0)
    silent = opt.get('silent', False)
    fs = opt.get('fastsearch', False)
    rf = opt.get('random_effect', True)
    theta = opt.get('theta', .25)

    mY = np.mean(Y)
    stdY = np.std(Y, ddof=1)

    Y = (Y - mY) / stdY

    dlab = np.zeros(lab.shape)
    dlab[1:] = (lab[1:] - lab[:-1] != 0)
    dlab[0] = 1
    kn = np.expand_dims(np.cumsum(dlab), axis=1)

    ns = np.max(kn)

    dkn = np.zeros(kn.shape)

    dkn[:-1] = (kn[1:] - kn[:-1] != 0)
    dkn[-1] = 1

    nk = np.ones([int(ns), 1])
    tmin = np.zeros([int(ns), 1])

    for k in range(int(ns)):
        tmin[k] = np.min(kn == k + 1)
        nk[k] = np.sum(kn == k + 1)
    s = np.cumsum(nk)

    s = np.insert(s[:-1], 0, 1)

    s = s.astype(int)

    if np.prod(onsetTime.shape) == np.prod(t.shape):
        onsetTime = onsetTime[s]

    if np.prod(endTime.shape) == np.prod(t.shape):
        endTime = endTime[s]

    if 'covariates' in opt:
        ncov = util.numel(opt['covariates'])
    else:
        ncov = 0

    cvr = np.zeros([np.prod(Y.shape), ncov])

    for jj in range(ncov):
        if opt['covariates'][jj].shape[0] == ns:
            cvr[:, jj] = np.squeeze(opt['covariates'][jj](kn))
        else:
            cvr[:, jj] = np.squeeze(opt['covariates'][jj])

    mcvr = np.mean(cvr, 0)
    stdcvr = np.std(cvr, 0, ddof=0)

    for k in range(cvr.shape[1]):
        cvr[:, k] = (cvr[:, k] - mcvr[k]) / stdcvr[k]

    cst = np.ones([np.prod(Y.shape), 1])

    acc = step

    mt = np.mean(t)

    t = t - mt

    onsetTime = onsetTime - mt

    endTime = endTime - mt

    cDelta = cDelta - mt

    # Estimate onset time distribution if needed

    m1 = opt['paramOnset'][0] - mt
    sigma1 = opt['paramOnset'][1]
    lambda1 = opt['paramOnset'][2]

    stats = {}
    stats['m1'] = m1 + mt
    stats['sigma1'] = sigma1
    t1Imputed = copy(onsetTime)

    t1Imputed[grp == 1] = util.imputeOnset(endTime[grp == 1], m1, sigma1, 0)

    t1Imputed[grp == 3] = util.imputeOnsetAD(endTime[grp == 3], m1, sigma1, lambda1)

    shiftMin = 0
    shiftMax = 30

    # Fit Using Censored likelihood

    IC = grp == 1
    IMCI = grp == 2
    IAD = grp == 3

    kn = kn.astype(int)

    IC2 = np.squeeze(IC[kn - 1])

    yc = Y[IC2]
    tc = t[IC2]
    covc = cvr[IC2, :]
    Tc = endTime[IC]
    dknc = dkn[IC2]

    IMCI2 = np.squeeze(IMCI[kn - 1])
    ymci = Y[IMCI2]
    tmci = t[IMCI2]
    covmci = cvr[IMCI2, :]
    zmci = onsetTime[IMCI]
    dknmci = dkn[IMCI2]

    IAD2 = np.squeeze(IAD[kn - 1])
    yad = Y[IAD2]
    tad = t[IAD2]
    covad = cvr[IAD2, :]
    zad = onsetTime[IAD]

    dknad = dkn[IAD2]

    tcp = time.time() - tcp

    stats['normalShift'] = -1000

    [bc2, s02, r02] = util.groupRegression2(Y, np.hstack([cst, cvr, t, t1Imputed[np.squeeze(kn) - 1]]), kn)[1:4]

    init = {}
    init['b0'] = np.block([[bc2], [0]])
    init['s0'] = s02
    init['r0'] = r02

    lb = -np.inf * np.ones([ncov + 7, 1])

    ub = np.inf * np.ones([ncov + 7, 1])

    lb[ncov + 4] = shiftMin

    ub[ncov + 4] = 100

    lb[0] = -100

    ub[0] = 100
    if constrainb1 > 0.5:
        lb[1] = 0
    else:
        ub[1] = 0

    lb[-1] = 1e-6
    lb[-2] = 1e-6
    if not rf:
        ub[-2] = 1e-6

    basis = np.block([cst, cvr, t1Imputed[np.squeeze(kn) - 1]])
    t1I = t1Imputed[np.squeeze(kn) - 1]
    dL = 0.1

    lb0 = np.block([[lb[:ncov + 4]], [lb[ncov + 5:]]])
    ub0 = np.block([[ub[:ncov + 4]], [ub[ncov + 5:]]])

    first = True

    alls = np.array([-40, -30, -20, -10])
    alls = np.block(
        [alls, np.arange(-5, 4.1), np.arange(5, 30.1, 2.5), np.arange(35, 70.1, 5), np.arange(80, 160.1, 10)])

    if fs:
        alls = np.arange(-20, 10, 160.1)
    weights = np.zeros([util.numel(alls)])

    weights[1:-1] = (alls[2:] - alls[:-2]) / 2
    weights[0] = alls[1] - alls[0]
    weights[-1] = alls[-1] - alls[-2]

    lc = np.zeros([util.numel(alls)])
    sc = np.zeros([util.numel(alls)])
    kk = 0
    while True and kk < util.numel(alls):
        s = alls[kk]
        tt = util.shiftVar(t1I, s, t, grp)
        if np.sum(np.abs(tt) > 0):
            [b0, s0, r0] = util.groupRegression2(Y, np.block([basis, t, tt]), kn, init)[1:4]
            break

        kk += 1

    util.dknc = dknc
    util.yc = yc
    util.tc = tc
    util.covc = covc
    util.Tc = Tc
    util.m1 = m1
    util.sigma1 = sigma1
    util.theta = theta
    util.cDelta = cDelta
    util.dknmci = dknmci
    util.ymci = ymci
    util.tmci = tmci
    util.covmci = covmci
    util.zmci = zmci
    util.dknad = dknad
    util.yad = yad
    util.tad = tad
    util.covad = covad
    util.zad = zad
    util.lambda1 = lambda1

    # print(yad)

    kbad = 0

    bnds = []
    for i in range(len(ub0)):
        lbnow = lb0[i][0]
        ubnow = ub0[i][0]
        if np.isinf(lbnow):
            lbnow = None
        if np.isinf(ubnow):
            ubnow = None

        bnds += [(lbnow, ubnow)]

    for kk in range(util.numel(alls)):
        s = alls[kk]

        if first:
            param = [b0[0][0], b0[ncov + 2][0]] + [i[0] for i in b0[1:ncov + 2]] + [b0[ncov + 3][0], np.sqrt(r0),
                                                                                    np.sqrt(s0)]
            if param[4] > 0:
                param[4] = - param[4]

        # print(param,bnds)

        # res = sp.optimize.minimize(lambda x: util.funWithGrad(x, s, nargout=1), param, method='SLSQP',bounds=bnds)

        # param = [ -0.4054, -0.0449, 0.0364,0.0352, -2.3715,0.7048,0.2393]
        # s = - 20
        res = sp.optimize.minimize(lambda x: util.funWithGrad(x, s, nargout=1), param, \
                                   jac=lambda x: util.funWithGrad(x, s, nargout=2), method='SLSQP', bounds=bnds)

        param = res['x']
        lmin = res['fun']

        if first:
            LBase = - lmin
            first = False

        lc[kk] = -lmin

        sc[kk] = s

        S = lc[kbad:kk + 1] > LBase + dL

        if np.sum(S) > 0:
            dl = np.exp(lc[kbad:kk + 1] - LBase) - 1
            D = np.sum(dl * sc[kbad:kk + 1] * S * weights[kbad:kk + 1]) / np.sum(dl * S * weights[kbad:kk + 1])
        else:
            D = -1000

        jml = np.argmax(lc[kbad:kk + 1])

        if jml == kbad:
            ml = alls[kbad]
        elif jml == kk:
            ml = alls[kk]
            paramBest = param
        else:
            p = alls[jml] - alls[jml - 1]
            q = alls[jml + 1] - alls[jml]
            ba = lc[jml] - lc[jml - 1]
            bc = lc[jml] - lc[jml + 1]
            ml = alls[jml] + (q ** 2 * ba - p ** 2 * bc) / (2 * (p * bc + q * ba))

        if not silent:
            print(sc[kbad: kk + 1], lc[kbad: kk + 1] - lc[kbad])
            plt.rc('font', size=18)
            plt.rc('lines', lw=3)
            plt.plot(sc[kbad: kk + 1], lc[kbad: kk + 1] - lc[kbad], 'k')
            plt.xlabel('Delta')
            plt.ylabel('Log Likelihood')
            plt.draw()
            plt.pause(0.001)

        if s > 40 and lc[kk] < LBase + dL:
            break

    stats['likelihoodCurve'] = np.block([sc[kbad:kk + 1], lc[kbad:kk + 1]])
    stats['mapShift'] = D
    stats['shift'] = ml
    res = sp.optimize.minimize(lambda x: util.funWithGrad(x, D, nargout=1), paramBest, \
                               jac=lambda x: util.funWithGrad(x, D, nargout=2), method='SLSQP', bounds=bnds)

    paramMAP = res['x']

    paramMAP = np.block([paramMAP[:ncov + 4], D, paramMAP[ncov + 4: ncov + 6]])

    res = sp.optimize.minimize(lambda x: util.funWithGrad(x, ml, nargout=1), paramBest, \
                               jac=lambda x: util.funWithGrad(x, ml, nargout=2), method='SLSQP', bounds=bnds)

    paramBest = res['x']
    param = np.block([paramBest[:ncov + 4], ml, paramBest[ncov + 4: ncov + 6]])

    stats['regParam'] = stdY * param[[0] + list(range(2, ncov + 2)) + [1, ncov + 2, ncov + 3]]

    stats['shift'] = param[ncov + 4]
    stats['rho'] = stdY * param[ncov + 5]
    stats['sigma'] = stdY * param[ncov + 6]
    stats['param'] = stdY * param
    stats['paramMAP'] = stdY * paramMAP

    for k in range(ncov):
        stats['paramMAP'][k + 2] = stats['paramMAP'][k + 2] / stdcvr[k]
        stats['paramMAP'][0] = stats['paramMAP'][0] - mcvr[k] * stats['paramMAP'][k + 2]
        stats['param'][k + 2] = stats['param'][k + 2] / stdcvr[k]
        stats['param'][0] = stats['param'][0] - mcvr[k] * stats['param'][k + 2]
    stats['paramMAP'][0] = stats['paramMAP'][0] - (stats['paramMAP'][1] + stats['paramMAP'][ncov + 2]) * mt + mY
    stats['param'][0] = stats['param'][0] - (stats['param'][1] + stats['param'][ncov + 2]) * mt + mY
    stats['paramMAP'][ncov + 4] = stats['mapShift']
    stats['param'][ncov + 4] = stats['shift']
    print(stats)

    plt.show()

    return stats
