import numpy as np
import scipy as sp
import scipy.special
import scipy.stats
from scipy.integrate import quad as integrate


    

def numel(x):
    return np.prod(x.shape)

def normexp(u, a):
    #computes exp(-a)Phi(u)
    y = np.zeros(u.shape)
    I = a < -5
    J = not I
    y[I] = np.exp(-a[I] - u[I]**2/2) * sp.special.erfcx(-u[I]/np.sqrt(2))/2
    y[J] = np.exp(-a[J]) * sp.special.erfc(-u[J]/np.sqrt(2))/2
    return y


def imputeOnset(T1, m1, sigma1, alpha):
    #Sample onset times t1 for unobserved data
    if alpha > 1e-10:
        J0 = normexp((T1-m1)/sigma1 - sigma1/alpha, (T1 - m1)/alpha - sigma1**2/(2*alpha**2))
        J1 = 1 - sp.stats.norm.cdf((T1 - m1)/sigma1)
        J = J1 + J0
        z = (sigma1/np.sqrt(2*np.pi))*np.exp(-(T1-m1)**2/(2*sigma1**2)) - (T1-m1)*J1
        t1 = alpha + T1 + z/J
    else:
        z = (T1-m1)/sigma1
        t1 = m1 + sigma1 * sp.stats.norm.pdf(z)/(1-sp.stats.norm.cdf(z))
    return t1

def imputeOnsetAD(z, m1, sigma1, lambda1):
    #Sample onset times t1 for unobserved data
    u = (z-m1)/sigma1 - lambda1 * sigma1
    t1 = m1 + lambda1*sigma1**2 - sigma1 * sp.stats.norm.pdf(u)/(sp.stats.norm.cdf(u))
    return t1

def blocksum(X, D):
    s = np.expand_dims(np.cumsum(X), axis=1)
    s = s[np.squeeze(D)==1, :]
    s[1:, :] = s[1:, :] - s[:-1, :]
    return s

def regResidual(allJ, cvr):
    if allJ.dtype == np.dtype('O'):
        ns = numel(allJ)

        Sz = [ns, numel(allJ[0])]
        jac0 = np.zeros(Sz)

        for k in ns:
            jac0[k, :] = allJ[k]
    else:

        ns = allJ.shape[0]
        jac0 = allJ

    if len(cvr) == 0:
        jac = jac0
    else:
        if (cvr.shape[0] != ns):
            cvr = cvr.T
        cc = np.dot(cvr.T,cvr)
        b = np.dot(np.linalg.solve(cc,cvr.T) , jac0)
        jac = jac0 - np.dot(cvr , b)
    if allJ.dtype == np.dtype('O'):
        jacR = np.zeros((1,ns), dtype=np.object)
        for k in range(ns):
            jacR[k] = jac[k,:]
    else:
        jacR = jac

    return [jacR, b]


def groupRegression2(y, x, lab, *args):
    # y:  array, n x dimy,
    # x:  array, n x dimx
    # lab: subject labels. ordered
    # b: parameter, dimx x dimy

    dimy = y.shape[1]
    ns = np.max(lab)
    n = numel(lab)

    dlab = np.zeros(lab.shape)
    dlab[:-1] = ((lab[1:] - lab[:-1]) != 0)
    dlab[-1] = 1
    n1 = blocksum(np.ones(lab.shape), dlab)
    n1rep = n1[:, np.ones(dimy).astype(int)-1]
    maxIter = 50


    if len(args) == 0:


        b = regResidual(y,x)[1]

        RY = y - np.dot(x , b)
        sig2 = np.mean(RY**2)
        rho2 = sig2/2
        sig2 = sig2/2

    else:
        init = args[0]
        b = init['b0']
        RY = y - np.dot(x , b)
        rho2 = init['r0']
        sig2 = init['s0']



    ns = ns.astype(int)
    for it in range(maxIter):
        s = blocksum(RY, dlab)

        a = n1*rho2 + sig2*np.ones([ns,1]).astype(int)

        tau2 = rho2*sig2
        tau2 = tau2*np.ones([ns,1])/a
        c = rho2*np.ones([ns,1])/a
        m = c * s
        [R, b] = regResidual(y-m[np.squeeze(lab-1)], x)

        RY = y - np.dot(x , b)
        sR = blocksum(RY, dlab)
        mR = np.sum(c*sR**2)

        lik = (np.sum(RY**2) - mR) / (2*sig2)
        lik = lik + (n/2) * np.log(sig2) + (ns/2) * np.log(rho2) - 0.5 *np.sum(np.log(tau2))

        sig2 = (np.sum(R**2) + np.sum(n1rep*tau2))/n
        rho2 = (np.sum(m**2) + np.sum(tau2))/ns

    return [lik, b, sig2, rho2, m]

def shiftVar(t1, s, t, grp):
    tt = np.zeros(t1.shape)

    grpt = np.zeros(t1.shape).astype(bool)
    grpt[:grp.shape[0],:grp.shape[1]] = grp == 1
    b = t - t1
    b = b[grpt]
    tt[ grpt ] = (b > -s) * (b + s)
    return tt

def Stepfun(t,theta):
    I = t > 2 *theta
    J = t < 0
    IJ = np.logical_not(I) & np.logical_not(J)
    res = np.zeros(t.shape)
    res[I]=t[I]
    res[IJ] = -t[IJ]**3/(4*theta**2) + t[IJ]**2/theta
    return res

def Lognormcdf(x):
    J = x < -5
    JJ = np.logical_not(J)
    y = np.zeros(x.shape)
    y[J] = -x[J]**2/2+np.log(sp.special.erfcx(-x[J]/np.sqrt(2))/2)
    y[JJ] = np.log(sp.stats.norm.cdf(x[JJ]))
    return y




def calcLikelihood_gradient_c_smooth(ekn,y,x,T,param, m1,sigma1,alpha, theta, cDelta,nargout=0):

    if nargout == 2:
        withGradient = True
    else:
        withGradient = False
    #withGradient = True

    x = np.block(x)
    t=x[:,0]

    nc=x.shape[1]

    a = param[0]

    b1 = np.array([ [i] for i in param[1:1 + nc]])
    b2 = param[1 + nc]
    c = param[2 + nc]
    delta = param[3 + nc]
    rho = param[4 + nc]
    sigma = param[5 + nc]
    pDelta = 0.0



    Tmax = m1 + 4 * sigma1
    ekn = ekn.astype(bool)
    Ns = numel(y)
    nScan = blocksum(np.ones([Ns, 1]), ekn)

    ns = numel(nScan)

    rho2 = rho ** 2
    sigma2 = sigma ** 2
    gamma2 = nScan * rho2 + sigma2

    if alpha > 1e-10:
        L = - ns * np.log(alpha) - pDelta *delta
    else:
        L = -ns * np.log(sigma1) - pDelta * delta

    L = L - np.sum(np.log(gamma2))/2 - (Ns - ns) * np.log(sigma) - (Ns/2) * np.log(2*np.pi)

    if withGradient:
        dLrho = - np.sum(rho * nScan / gamma2)
        dLsigma = - np.sum(sigma / gamma2) - (Ns - ns) / sigma
        dLa = 0
        dLb1 = np.zeros([nc, 1])
        dLb2 = 0
        dLc = 0



    rj = y - a - np.dot(x,b1)
    kn = 0
    def stepfun(t,u0):

        u = max(u0,delta+cDelta)

        res = t - u

        res = Stepfun(res+delta,theta)
        return res

    for k in range(ns):
        p=int(nScan[k])
        tk = t[kn:kn+p]
        rk = rj[kn:kn+p]
        xk=x[kn:kn+p,:]
        kn=kn+p

        gamma2k = gamma2[k,0]

        muk = 1/gamma2k

        muk2 = rho2 / (gamma2k * sigma2)


        if withGradient:
            allI = np.zeros([8+nc,1])


            def kthFun(u,kf):
                rl = rk.reshape((1,numel(tk)))
                tl = tk.reshape((1,numel(tk)))


                st = stepfun(tl,u)
                resj = rl - b2 * u - c*st
                mj = np.mean(resj)
                mj2 = resj - mj
                mj = mj **2 * resj.shape[1]
                res0 = np.sum(mj2**2)/sigma2+muk * mj
                if alpha > 1e-10:
                    res = np.exp(-res0/2) * normexp((u-m1)/alpha-sigma1**2/2*alpha, (u-m1)/sigma1-sigma1/alpha)
                else:
                    res = np.exp(-res0/2 - .5*((u-m1)/sigma1)**2)/(np.sqrt(2*np.pi))
                # print('u=%f,res=%f,kf=%f' % (u, res, kf))
                # print('st=',st)
                # print('rk=',rk)
                if kf == 0:

                    return res
                elif kf == 1:
                    res = u * res
                elif kf == 2:
                    res = u **2 *res
                elif kf == 3:
                    res = u * np.sum(st)* res
                elif kf == 4:
                    res = np.dot(st , rk )* res
                elif kf > 4 and kf < 5 + nc:
                    res = np.dot(st ,xk[:,kf-5])*res

                elif kf == 5 + nc:
                    res = np.sum(st)*res

                elif kf == 6+nc:
                    res = np.sum(st**2) * res
                elif kf == 7+nc:
                    res = np.sum(st)**2 * res
                return res




            for kf in range(8+nc):


                # newfun = lambda x:kthFun(x,kf)
                # print('kf= %f' % kf)
                # allI[kf,0] = 0
                #integrate(newfun, T[k], Tmax)
                allI[kf,0] = integrate(lambda x:kthFun(x,kf), T[k], Tmax)[0]
            I0 = allI[0,0]
            Iu = allI[1,0]
            Iu2 = allI[2,0]
            Ipsiu = allI[3,0]
            Ipsir = allI[4,0]
            Ipsix = np.zeros([nc,1])
            for j in range(nc):
                Ipsix[j] = allI[5+j,0]
            Ipsim = allI[5+nc,0]
            Ipsi2m = allI[6+nc,0]
            Ipsim2 = allI[7+nc,0]
        else:

            def phi0(u):

                rl = rk.reshape((1,numel(tk)))
                tl = tk.reshape((1,numel(tk)))

                st = stepfun(tl,u)
                resj = rl - b2 * u - c*st
                mj = np.mean(resj)
                mj2 = resj - mj
                mj = mj **2 * resj.shape[1]
                res0 = np.sum(mj2**2)/sigma2+muk * mj
                if alpha > 1e-10:
                    res = np.exp(-res0/2) * normexp((u-m1)/alpha-sigma1**2/2*alpha, (u-m1)/sigma1-sigma1/alpha)
                else:
                    res = np.exp(-res0/2 - .5*((u-m1)/sigma1)**2)/(np.sqrt(2*np.pi))
                return res



            I0 = integrate(phi0,T[k],Tmax)[0]

        if withGradient:
            brk = np.sum(rk)
            bxk = np.sum(xk,0).reshape((-1,1))

            dIa = (brk * I0 - p * b2 * Iu - c * Ipsim) / gamma2k
            dIb1 = (np.dot(xk.T,rk)/sigma2 - muk2*brk*bxk)*I0 - bxk*b2*Iu/gamma2k - c * (Ipsix / sigma2 - muk2 * Ipsim * bxk);

            dIb2 = (brk * Iu - p * b2 * Iu2 - c * Ipsiu) / gamma2k
            dIc = Ipsir / sigma2 - muk2 * Ipsim * brk - b2 * Ipsiu / gamma2k - c * (Ipsi2m / sigma2 - muk2 * Ipsim2)

            tmp = (brk ** 2 * I0 - 2 * brk * ( b2 * p * Iu + c * Ipsim) + b2 ** 2 * p ** 2 * Iu2 + 2 * p * b2 * c * Ipsiu + c ** 2 * Ipsim2)

            dIsigma2 = (np.sum(  rk ** 2) * I0 - 2 * b2 * brk * Iu - 2 * c * Ipsir + p * b2 ** 2 * Iu2 + 2 * b2 * c * Ipsiu + c ** 2 * Ipsi2m) \
                       / ( 2 * sigma2 ** 2) - (0.5 / p) * (1 / sigma2 ** 2 - 1 / gamma2k ** 2) * tmp

            dIsigma = 2 * sigma * dIsigma2
            dIrho2 = tmp / (2 * gamma2k ** 2)
            dIrho = 2 * rho * dIrho2


        L = L + np.log(I0)

        if withGradient:
            dLa = dLa + dIa/I0
            dLb1 = dLb1 + dIb1 / I0
            dLsigma = dLsigma + dIsigma / I0
            dLrho = dLrho + dIrho / I0
            dLb2 = dLb2 + dIb2 / I0
            dLc = dLc + dIc / I0


    if withGradient:
        dL = np.block([dLa,dLb1.T, dLb2,dLc,dLrho,dLsigma])
        return dL
    else:
        return L




def calcLikelihood_gradient_mci_smooth(ekn,y,x,u,param, m1,sigma1,alpha, theta, cDelta,nargout=0):

    if nargout == 2:
        withGradient = True
    else:
        withGradient = False
    #withGradient = True

    x = np.block(x)
    t=x[:,0]

    nc=x.shape[1]

    ekn = ekn.astype(bool)



    Ns = numel(y)
    nScan = blocksum(np.ones([Ns, 1]), ekn)

    ns = numel(nScan)
    nNs = ns + Ns

    ekn2 = np.zeros((nNs,1)).astype(bool)

    ekn2range = np.cumsum(nScan)+np.arange(ns)
    ekn2range = ekn2range.astype(int)
    ekn2[ekn2range] = True

    bkn2 = np.zeros((nNs,1)).astype(bool)
    bkn2[1:] = ekn2[:-1]
    bkn2[0] = True
    nbkn2 = np.logical_not(bkn2)
    kn2 = 1 + np.cumsum(ekn2)
    kn2[1:]=kn2[:-1]
    kn2[0]=1

    ys = np.zeros((nNs,1))
    ys[nbkn2] = y.reshape(ys[nbkn2].shape)

    ts = np.zeros((nNs,1))
    ts[nbkn2] = t.reshape(ts[nbkn2].shape)

    xs = np.zeros((nNs,nc))


    xs[nbkn2.reshape(-1,),:] = x

    us = u[kn2-1]

    rho = param[4+nc]
    sigma = param[5+nc]
    pDelta = 0.0

    gamma2 = nScan * rho ** 2 + sigma ** 2

    if withGradient:
        dgamma2rho = 2 * rho * nScan
        dgamma2sigma = 2 * sigma

    stepTime = Stepfun(ts - np.maximum(us - param[3 + nc], cDelta).reshape((-1,1)), theta)


    rj = ys- param[0] - np.dot(xs , param[1:1+nc]).reshape((-1,1))  - us.reshape((-1,1)) * param[1 + nc] - param[2 + nc] * stepTime

    rj[bkn2] = 0

    Aj = np.cumsum(rj)

    Aj = Aj[ekn2.reshape((-1,))] - Aj[bkn2.reshape((-1,))]
    Ajm = Aj.reshape((-1,1))/nScan


    rjm = rj - Ajm[kn2-1]
    rjm[bkn2.reshape(-1,),:] = 0


    tmp = - np.block([np.ones((nNs,1)),xs,us.reshape((-1,1)),stepTime])
    tmp[bkn2.reshape(-1,),:] = 0


    if withGradient:
        drj2 = 2 * rj * tmp
        drj2a = drj2[:,0]
        drj2b1 = drj2[:,1:-2]
        drj2b2 = drj2[:,-2]
        drj2c = drj2[:,-1]
        dAj = np.cumsum(tmp,0)
        dAj = dAj[ekn2.reshape(-1,),:]-dAj[bkn2.reshape(-1,),:]
        dAja = dAj[:,0]

        dAjb1 = dAj[:,1:-2]
        dAjb2 = dAj[:,-2]
        dAjc = dAj[:,-1]


    if alpha > 1e-10:

        L = -np.sum(np.log(gamma2))/2 - np.sum(u)/alpha + ns * (m1/alpha) + ns *(sigma1**2/(2*alpha**2)) - ns*np.log(alpha) - \
        0.5*Ns*np.log(2*np.pi) - (Ns-ns) * np.log(sigma) - np.sum(rjm**2)/(2*sigma**2) -  \
        0.5*np.sum(Aj.reshape((-1,1))**2/(nScan*gamma2))  + np.sum(Lognormcdf((u-m1)/sigma1 - sigma1/alpha)) - pDelta * param[3+nc]

    else:
        L = - np.sum(np.log(gamma2)) / 2 - ns * np.log(sigma1) - 0.5 * (Ns + ns) * np.log(2 * np.pi) - \
        (Ns - ns) * np.log(sigma) - np.sum(rjm ** 2) / (2 * sigma ** 2) - \
        0.5 * np.sum(Aj.reshape((-1,1)) ** 2 / (nScan * gamma2)) - 0.5 * np.sum(((u - m1) / sigma1) ** 2) - pDelta * param[3 + nc]


    if withGradient:
        dLa = - np.sum(drj2a) / (2 * sigma ** 2) + (rho ** 2 / sigma ** 2) * np.sum(dAja.reshape((-1,1)) * Aj.reshape((-1,1)) / gamma2)

        dLb1 = - np.sum(drj2b1, 0) / (2 * sigma ** 2) + (rho ** 2 / sigma ** 2) * np.sum(dAjb1 * \
                                                                                Aj.reshape((-1,1)) / gamma2, 0)

        dLb2 = - np.sum(drj2b2) / (2 * sigma ** 2) + (rho ** 2 / sigma ** 2) * np.sum(dAjb2.reshape((-1,1)) * Aj.reshape((-1,1)) / gamma2)

        dLc = - np.sum(drj2c) / (2 * sigma ** 2) + (rho ** 2 / sigma ** 2) * np.sum(dAjc.reshape((-1,1)) * Aj.reshape((-1,1)) / gamma2)

        dLrho = - np.sum(dgamma2rho.reshape((-1,1)) / gamma2) / 2 + (rho / sigma ** 2) * np.sum(Aj.reshape((-1,1)) ** 2 / gamma2)\
        - (rho ** 2 / sigma ** 2) * np.sum(dgamma2rho.reshape((-1,1)) * (Aj.reshape((-1,1)) ** 2) / gamma2 ** 2) / 2

        dLsigma = - np.sum(dgamma2sigma.reshape((-1,1)) / gamma2) / 2 - (rho ** 2 / sigma ** 3) * sum(Aj.reshape((-1,1)) ** 2 / gamma2)\
        - (rho ** 2 / sigma ** 2) * np.sum(dgamma2sigma.reshape((-1,1)) * (Aj.reshape((-1,1)) ** 2) / gamma2 ** 2) / 2 - (Ns - ns) / sigma + np.sum(\
            rj.reshape((-1,1)) ** 2) / (sigma ** 3)

        dL = np.block([dLa, dLb1, dLb2, dLc, dLrho, dLsigma])

    if withGradient:
        return dL
    else:
        return L



def calcLikelihood_gradient_ad_smooth(ekn,y,x,u1,param, m1,sigma1, Lambda, theta, cDelta,nargout=0):


    if nargout == 2:
        withGradient = True
    else:
        withGradient = False
    #withGradient = True

    x = np.block(x)
    t=x[:,0]

    nc=x.shape[1]

    a = param[0]

    b1 = np.array([ [i] for i in param[1:1 + nc]])
    b2 = param[1 + nc]
    c = param[2 + nc]
    delta = param[3 + nc]
    rho = param[4 + nc]
    sigma = param[5 + nc]
    pDelta = 0.0

    Tmin = m1 - 4 * sigma1
    ekn = ekn.astype(bool)
    Ns = numel(y)
    nScan = blocksum(np.ones([Ns, 1]), ekn)

    ns = numel(nScan)

    rho2 = rho ** 2
    sigma2 = sigma ** 2
    gamma2 = nScan * rho2 + sigma2

    L = - ns * np.log(sigma1) + ns * np.log(Lambda) - pDelta * delta



    L = L - np.sum(np.log(gamma2)) / 2 - (Ns - ns) * np.log(sigma) - (Ns / 2) * np.log(2 * np.pi) - \
    Lambda * np.sum(u1)

    if withGradient:
        dLrho = - np.sum(rho * nScan / gamma2)
        dLsigma = - np.sum(sigma / gamma2) - (Ns - ns) / sigma
        dLa = 0
        dLb1 = np.zeros([nc, 1])
        dLb2 = 0
        dLc = 0



    rj = y - a - np.dot(x,b1)
    kn = 0


    def stepfun(t,u0):

        u = max(u0,delta+cDelta)

        res = t - u

        res = Stepfun(res+delta,theta)
        return res

    for k in range(ns):
        p=int(nScan[k])
        tk = t[kn:kn+p]
        rk = rj[kn:kn+p]
        xk=x[kn:kn+p,:]
        kn=kn+p

        gamma2k = gamma2[k,0]

        muk = 1/gamma2k

        muk2 = rho2 / (gamma2k * sigma2)


        if withGradient:
            allI = np.zeros([8+nc,1])


            def kthFun(u,kf):
                rl = rk.reshape((1,numel(tk)))
                tl = tk.reshape((1,numel(tk)))

                st = stepfun(tl,u)
                resj = rl - b2 * u - c*st
                mj = np.mean(resj)
                mj2 = resj - mj
                mj = mj **2 * resj.shape[1]
                res0 = np.sum(mj2**2)/sigma2+muk * mj

                res = np.exp(-res0/2 - .5*((u-m1)/sigma1)**2 + Lambda * u)/(np.sqrt(2*np.pi))
                # print('u=%f,res=%f,kf=%f' % (u, res, kf))
                # print('st=',st)
                # print('rk=',rk)
                if kf == 0:

                    return res
                elif kf == 1:
                    res = u * res
                elif kf == 2:
                    res = u **2 *res
                elif kf == 3:
                    res = u * np.sum(st)* res
                elif kf == 4:
                    res = np.dot(st , rk )* res
                elif kf > 4 and kf < 5 + nc:
                    res = np.dot(st ,xk[:,kf-5])*res

                elif kf == 5 + nc:
                    res = np.sum(st)*res

                elif kf == 6+nc:
                    res = np.sum(st**2) * res
                elif kf == 7+nc:
                    res = np.sum(st)**2 * res
                return res




            for kf in range(8+nc):


                # newfun = lambda x:kthFun(x,kf)
                # print('kf= %f' % kf)
                # allI[kf,0] = 0
                #integrate(newfun, T[k], Tmax)


                allI[kf,0] = integrate(lambda x:kthFun(x,kf), Tmin, u1[k])[0]

            I0 = allI[0,0]
            Iu = allI[1,0]
            Iu2 = allI[2,0]
            Ipsiu = allI[3,0]
            Ipsir = allI[4,0]
            Ipsix = np.zeros([nc,1])
            for j in range(nc):
                Ipsix[j] = allI[5+j,0]
            Ipsim = allI[5+nc,0]
            Ipsi2m = allI[6+nc,0]
            Ipsim2 = allI[7+nc,0]
        else:

            def phi0(u):
                rl = rk.reshape((1,numel(tk)))
                tl = tk.reshape((1,numel(tk)))

                st = stepfun(tl,u)
                resj = rl - b2 * u - c*st
                mj = np.mean(resj)
                mj2 = resj - mj
                mj = mj **2 * resj.shape[1]
                res0 = np.sum(mj2**2)/sigma2+muk * mj

                res = np.exp(-res0/2 - .5*((u-m1)/sigma1)**2 + Lambda * u)/(np.sqrt(2*np.pi))
                return res



            I0 = integrate(phi0,Tmin,u1[k])[0]
        if withGradient:
            brk = np.sum(rk)
            bxk = np.sum(xk,0).reshape((-1,1))

            dIa = (brk * I0 - p * b2 * Iu - c * Ipsim) / gamma2k
            dIb1 = (np.dot(xk.T,rk)/sigma2 - muk2*brk*bxk)*I0 - bxk*b2*Iu/gamma2k - c * (Ipsix / sigma2 - muk2 * Ipsim * bxk);

            dIb2 = (brk * Iu - p * b2 * Iu2 - c * Ipsiu) / gamma2k
            dIc = Ipsir / sigma2 - muk2 * Ipsim * brk - b2 * Ipsiu / gamma2k - c * (Ipsi2m / sigma2 - muk2 * Ipsim2)

            tmp = (brk ** 2 * I0 - 2 * brk * ( b2 * p * Iu + c * Ipsim) + b2 ** 2 * p ** 2 * Iu2 + 2 * p * b2 * c * Ipsiu + c ** 2 * Ipsim2)

            dIsigma2 = (np.sum(  rk ** 2) * I0 - 2 * b2 * brk * Iu - 2 * c * Ipsir + p * b2 ** 2 * Iu2 + 2 * b2 * c * Ipsiu + c ** 2 * Ipsi2m) \
                       / ( 2 * sigma2 ** 2) - (0.5 / p) * (1 / sigma2 ** 2 - 1 / gamma2k ** 2) * tmp

            dIsigma = 2 * sigma * dIsigma2
            dIrho2 = tmp / (2 * gamma2k ** 2)
            dIrho = 2 * rho * dIrho2

        L = L + np.log(I0)

        if withGradient:
            dLa = dLa + dIa/I0
            dLb1 = dLb1 + dIb1 / I0
            dLsigma = dLsigma + dIsigma / I0
            dLrho = dLrho + dIrho / I0
            dLb2 = dLb2 + dIb2 / I0
            dLc = dLc + dIc / I0


    if withGradient:
        dL = np.block([dLa,dLb1.T, dLb2,dLc,dLrho,dLsigma])
        return dL
    else:
        return L





def funWithGrad(x,delta,nargout=0):
    paramloc = list(x[:-2])+[delta]+list(x[-2:])



    LdL = calcLikelihood_gradient_c_smooth(dknc,yc,[tc,covc],Tc, paramloc, m1,sigma1,0, theta, cDelta,nargout=nargout)




    LdL1 = calcLikelihood_gradient_mci_smooth(dknmci, ymci, [tmci, covmci], zmci, paramloc, m1, sigma1, 0, theta, cDelta, nargout=nargout)




    LdL2 = calcLikelihood_gradient_ad_smooth(dknad, yad, [tad, covad], zad, paramloc, m1, sigma1, lambda1, theta,
                                             cDelta, nargout=nargout)


    LdL = -LdL -LdL1 -LdL2


    return LdL





