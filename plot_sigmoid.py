import numpy as np
import pandas as pd
from sigmoid import SigmoidalRegression, sigFunction
import matplotlib
matplotlib.use("QT5Agg")
import matplotlib.pyplot as plt
plt.ioff()
nsub = 500
npersub = 5
nobs = npersub * np.ones(nsub)
N = nsub*npersub
t0 = 30
t1 = 50
a = -200
c = 0.5
d = 0.5
# c=0
# d=0
b = -0.6
rho = 250
sigma = 100

t = np.linspace(0, 80, 1000)
Y = sigFunction(t, a, b, c, d, (t0+t1)/2,  t1-t0)
plt.plot(t,Y)
plt.show()